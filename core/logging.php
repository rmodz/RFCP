<?php

function gamecp_log($log_level, $log_account, $log_message, $disable_return = 0)
{
  global $userdb;
  global $gamecpdb;
  global $datadb;
  global $user2db;
  global $itemsdb;
  global $gamecp_dbconnect;
  global $data_dbconnect;
  global $user_dbconnect;
  global $items_dbconnect;

  $log_time = time();
  $log_ip = $_SERVER['REMOTE_ADDR'];
  $log_browser = antiject($_SERVER['HTTP_USER_AGENT']);
  $log_page = antiject($_SERVER['REQUEST_URI']);
  $log_account = str_replace(";\$", "", $log_account);
  $log_account = str_replace("\\\\", "", $log_account);
  $log_message = str_replace(";\$", "", $log_message);
  $log_message = str_replace("\\\\", "", $log_message);
  if ($log_account != "" && $log_message != "")
  {
    $sql = "INSERT INTO gamecp_log (log_level, log_time, log_account, log_message, log_ip, log_page, log_browser) VALUES (?,?,?,?,?,?,?)";
    $params = array ($log_level, $log_time, $log_account, $log_message, $log_ip,
      $log_page, $log_browser);
    $gamecploq_query = sqlsrv_query(connectdb(RFCP), $sql, $params);
    close_db(RFCP);
  }
}

function name_change_log($charserial, $oldname, $newname, $ip)
{
  $sql = "INSERT INTO gamecp_username_log (username_charid, username_oldname, username_newname, username_ip) VALUES (?,?,?,?)";
  $params = array ($charserial, $oldname, $newname, $ip);
  $name_change_log_query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
  @sqlsrv_free_stmt($name_change_log_query);
  close_db(RFCP);
}

function new_register_log($username, $date, $ip, $browser)
{
  $sql = "INSERT INTO gamecp_registration_log (reg_account, reg_ip, reg_time, reg_browser) VALUES (?,?,?,?)";
  $params = array ($username, $ip, $date, $browser);
  $new_register_log_query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
  @sqlsrv_free_stmt($new_register_log_query);
  close_db(RFCP);
}

function vote_log($vote_id, $id, $ip, $gained, $total)
{
  $time = time();
  $sql = "INSERT INTO gamecp_vote_log (site_id, log_account_serial, log_time, log_ip, log_points_gained, log_total_points) VALUES (?,?,?,?,?,?)";
  $params = array ($vote_id, $id, $time, $ip, $gained, $total);
  $vote_log_query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
  @sqlsrv_free_stmt($vote_log_query);
  close_db(RFCP);
}

function item_redeem_log($account_id, $char_id, $price, $item_id, $item_name, $item_amount, $item_dbcode, $total_credits)
{
  $time = time();
  $sql = "INSERT INTO gamecp_redeem_log (redeem_account_id,redeem_char_id,redeem_price,redeem_item_id,redeem_item_name,redeem_item_amount,redeem_item_dbcode,redeem_total_gp,redeem_time) VALUES (?,?,?,?,?,?,?,?,?)";
  $params = array ($account_id, $char_id, $price, $item_id, $item_name, $item_amount,
    $item_dbcode, $total_credits, $time);
  $item_log_query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
  @sqlsrv_free_stmt($item_log_query);
  close_db(RFCP);
}
function item_redeem_g_log($account_id, $char_id, $price, $item_id, $item_name, $item_amount, $item_dbcode, $total_credits)
{
  $time = time();
  $sql = "INSERT INTO gamecp_g_redeem_log (redeem_account_id,redeem_char_id,redeem_price,redeem_item_id,redeem_item_name,redeem_item_amount,redeem_item_dbcode,redeem_total_gp,redeem_time) VALUES (?,?,?,?,?,?,?,?,?)";
  $params = array ($account_id, $char_id, $price, $item_id, $item_name, $item_amount,
    $item_dbcode, $total_credits, $time);
  $item_log_query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
  @sqlsrv_free_stmt($item_log_query);
  close_db(RFCP);
}
function item_delete_log($account_id, $char_id, $k, $u, $d, $r, $remover_id)
{
  $time = time();
  $sql = "INSERT INTO gamecp_delete_item_log (delete_account_id,delete_char_id,delete_k,delete_u,delete_d,delete_r,delete_remover_id,delete_time) VALUES (?,?,?,?,?,?,?,?)";
  $params = array ($account_id, $char_id, $k, $u, $d, $r, $remover_id, $time);
  $item_delete_query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
  @sqlsrv_free_stmt($item_delete_query);
  close_db(RFCP);
}

function bonus_item_log($account_id, $char_id, $k, $u, $d, $t, $txn_id)
{
  $time = time();
  $sql = "INSERT INTO gamecp_bonus_items_log (bonus_account_id,bonus_char_id,bonus_k,bonus_u,bonus_d,bonus_t,bonus_time, bonus_pp_trans) VALUES (?,?,?,?,?,?,?,?)";
  $params = array ($account_id, $char_id, $k, $u, $d, $t, $time, $txn_id);
  $item_bonus_query = @sqlsrv_query(connectdb(RFCP), $sql, $params);
  @sqlsrv_free_stmt($item_bonus_query);
  close_db(RFCP);
}
