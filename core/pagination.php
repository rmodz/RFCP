<?php

class Pagination
{

  public $connection_name = null;
  public $sql = null;
  public $sql_count = null;
  public $params = null;
  public $params_count = null;
  public $page_size = null;
  public $links_to_show = null;
  public $page = 1;
  public $row_count = null;
  public $max_pages = null;
  public $url = null;

  public function Pagination($connection_name, $sql, $sql_count, $url, $params = array (), $params_count = array (), $page_size = 10, $links_to_show = 10)
  {
    $this -> connection_name = $connection_name;
    $this -> sql = $sql;
    $this -> params = $params;
    $this -> page_size = $page_size;
    $this -> links_to_show = $links_to_show;
    $this -> sql_count = $sql_count;
    $this -> params_count = $params_count;
    $this -> url = $url;

    if (isset($_GET['page_gen']))
    {
      $this -> page = intval($_GET['page_gen']);
    }
  }

  public function get_data()
  {
    $return = array (
      "error" => False,
      "errorMessage" => "",
      "rows" => array ()
    );
    $count_query = sqlsrv_query(connectdb($this -> connection_name), $this -> sql_count, $this -> params_count);

    if (!$count_query || !($row = sqlsrv_fetch_array($count_query)))
    {
      $return["error"] = True;
      $return["errorMessage"] = "Count query failed";
    }
    else
    {
      $this -> row_count = intval($row[0]);
      $this -> max_pages = ceil($this -> row_count / $this -> page_size);
      if ($this -> max_pages < $this -> page || $this -> page < 0)
      {
        $this -> page = 1;
      }
      $this -> offset = $this -> page_size * ($this -> page - 1);
      $this -> sql = preg_replace("/SELECT/", "SELECT TOP {$this -> page_size}", $this -> sql, 1);
      $this -> sql = str_replace("[OFFSET]", (string) $this -> offset, $this -> sql);
      $results = @sqlsrv_query(connectdb($this -> connection_name), $this -> sql, $this -> params);
      if (!$results)
      {
        $return["error"] = True;
        $return["errorMessage"] = "Paging query failed";
      }
      else
      {
        while ($row = sqlsrv_fetch_array($results))
        {
          $return["rows"][] = $row;
        }
      }
    }

    return $return;
  }

  public function renderFirst($tag = "First")
  {
    $html = "";
    if ($this -> page == 1)
    {
      $html .= "<li class=\"previous disabled\">";
    }
    else
    {
      $html .= "<li class=\"previous\">";
    }

    $html .= "<a href=\"" . $this -> url . "&page_gen=1\">" . $tag . "</a>";
    $html .= "</li>";
    return $html;
  }

  public function renderLast($tag = "Last")
  {
    $html = "";
    if ($this -> page == $this -> max_pages)
    {
      $html .= "<li class=\"next disabled\">";
    }
    else
    {
      $html .= "<li class=\"next\">";
    }

    $html .= "<a href=\"" . $this -> url . "&page_gen=" . $this -> max_pages . "\">" . $tag . "</a>";
    $html .= "</li>";
    return $html;
  }

  public function renderNext($tag = "&gt;")
  {
    $html = "";
    $page = $this -> page + 1;
    if ($this -> page == $this -> max_pages)
    {
      $html .= "<li class=\"disabled\">";
      $page = $this -> max_pages;
    }
    else
    {
      $html .= "<li class=\"\">";
    }

    $html .= "<a href=\"" . $this -> url . "&page_gen=" . $page . "\">" . $tag . "</a>";
    $html .= "</li>";

    return $html;
  }

  public function renderPrev($tag = "&lt;")
  {
    $html = "";
    $page = $this -> page - 1;
    if ($this -> page == 1)
    {
      $html .= "<li class=\"disabled\">";
      $page = 1;
    }
    else
    {
      $html .= "<li class=\"\">";
    }

    $html .= "<a href=\"" . $this -> url . "&page_gen=" . $page . "\">" . $tag . "</a>";
    $html .= "</li>";

    return $html;
  }

  public function renderNav()
  {
    $start = 1;
    if ($this -> max_pages > $this -> links_to_show)
    {
      $middle = floor($this -> links_to_show / 2);
      if ($middle + $this -> page > $this -> max_pages)
      {
        $end = $this -> max_pages + 1;
        $start = $this -> max_pages - $this -> links_to_show + 1;
      }
      else if ($this -> page - $middle < 1)
      {
        $start = 1;
        $end = $this -> links_to_show + 1;
      }
      else
      {
        $start = $this -> page - $middle;
        $end = $start + $this -> links_to_show;
      }
    }
    else
    {
      $start = 1;
      $end = $this -> max_pages + 1;
    }

    $links = "";
    $i = $start;
    while ($i < $end)
    {
      if ($i == $this -> page)
      {
        $links .= " <li class=\"active\"><a href=\"" . $this -> url . "&page_gen=" . $i . "\">" . $i . "</a></li>";
      }
      else
      {
        $links .= " <li><a href=\"" . $this -> url . "&page_gen=" . $i . "\">" . $i . "</a></li>";
      }
      ++$i;
    }
    return $links;
  }

  public function renderFullNav()
  {
    $pager = "<nav class=\"ink-navigation push-center main_pager\">";
    $pager .= "<ul class=\"pagination rounded shadowed " . BASE_COLOR . " \">";
    $pager .= $this -> renderFirst();
    $pager .= $this -> renderPrev();
    $pager .= $this -> renderNav();
    $pager .= $this -> renderNext();
    $pager .= $this -> renderLast();
    $pager .= "</ul>";
    $pager .= "</nav>";
    $pager .= "<script type=\"text/javascript\">$(function(){center_pager();})</script>";
    return $pager;
  }

  public function getCurrentPage()
  {
    return $this -> page;
  }

}
