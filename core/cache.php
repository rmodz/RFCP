<?php

session_start();

if (function_exists("apc_add"))
{
  define("CACHE_METHOD", "APC");
}
else
{
  define("CACHE_METHOD", "NONE");
}

function add_cache($key, $value, $time_s)
{
  if (CACHE_METHOD == "APC")
  {
    apc_store($key, $value, $time_s);
  }
  else
  {
    $_SESSION[$key] = $value;
  }
}

function get_cache($key)
{
  if (CACHE_METHOD == "APC")
  {
    return apc_fetch($key);
  }
  elseif (CACHE_METHOD == "NONE" && isset($_SESSION[$key]))
  {
    return $_SESSION[$key];
  }
  else
  {
    return False;
  }
}

function clear_all_cache()
{
  if (CACHE_METHOD == "APC")
  {
    apc_clear_cache();
    apc_clear_cache("user");
    apc_clear_cache("opcode");
  }
  unset($_SESSION);
}

function delete_cache($key)
{
  if (CACHE_METHOD == "APC")
  {
    apc_delete($key);
  }
  elseif (CACHE_METHOD == "NONE")
  {
    unset($_SESSION);
  }
}
