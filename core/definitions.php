<?php

define("REGEX_USERNAME", "/^(^[a-zA-Z][a-zA-Z0-9]+)$/");
define("REGEX_PASSWORD", "/^([a-zA-Z0-9!@#$%^&*]+)$/");

//Notification Types
define("ERROR", "error");
define("INFO", "info");
define("WARNING", "");
define("SUCCESS", "success");

//Dealing with redirect
define("PREVIOUS_PAGE_SHORT", "1");
define("PREVIOUS_PAGE_LONG", "2");
define("CUSTOM_PAGE_SHORT", "3");
define("CUSTOM_PAGE_LONG", "4");
define("INDEX_PAGE_SHORT", "5");
define("REDIRECT_SHORT_TIME", "1");
define("REDIRECT_LONG_TIME", "3");

//Common messages
define("PAGE_NOT_FOUND", "Invalid page");
define("INVALID_PERMISSION", "You do not have permission to access this page");
define("INVALID_LOAD", "Invalid page load");

define("BASE_CODE", 268435455);
$upgrade_types = array (0, 1, 2, 3, 4, 5, 6, 7);

define("OUTPUT_HTML", 1);
define("OUTPUT_JSON", 2);

define("CONFIRM_EMAIL_CONTENTS", "Hello %username%\n Thank you for registering. Please link the link below (or copy it to your address bar) to complete the registration process and choose your account password:\n\nUsername: %username%\nConfirmation URL: %confirm_url%\n\nThank you");
