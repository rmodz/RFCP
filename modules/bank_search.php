<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Support";
  $moduleLabel = "Bank Search";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{

  /**
   * @author Mauro Tamm   @ mauro.tamm@gmail.com
   * @author Agony @ agony@nxtdeveloper.com
   * @copyright 2014 http://nxtdeveloper.com/
   * @ver 4.0
   */
  if ($userdata -> has_permission($action))
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $do_search = isset($_GET['do_search']) ? $_GET['do_search'] : "";
    $page_size = isset($_GET['page_size']) && ctype_digit($_GET['page_size']) ? intval($_GET['page_size']) : 10;
    $item_slots = isset($_GET['item_slots']) && ctype_digit($_GET['item_slots']) ? antiject($_GET['item_slots']) : 0;
    $item_upgrades = isset($_GET['talics']) ? $_GET['talics'] : array ();
    $item_id = isset($_GET['item_id']) ? antiject(trim($_GET['item_id'])) : "";
    $item_amount_max = isset($_GET['item_amount_max']) && ctype_digit($_GET['item_amount_max']) ? intval($_GET['item_amount_max']) : 0;
    $item_amount_min = isset($_GET['item_amount_min']) && ctype_digit($_GET['item_amount_max']) ? intval($_GET['item_amount_min']) : 0;
    $account_serial = isset($_GET['account_serial']) && ctype_digit($_GET['account_serial']) ? intval($_GET['account_serial']) : 0;
    $account_name = isset($_GET['account_name']) ? $_GET['account_name'] : "";
    $search_query = "";
    $num_of_bags = 100;
    $enable_itemsearch = false;
    $item_upgrade = 268435455;
    if (empty($page))
    {
      $out .= "<form class='ink-form' method='get' action='" . $script_name . "?action=" . $_GET['action'] . "'>";
      $out .= "<table class='ink-form' cellpadding='3' cellspacing='1' border='0' width='100%' style='font-size: 12pt;'>";
      $out .= "<tr>";
      $out .= "<td colspan='2' style=''><b>Search Bank</b></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Account Serial:</td>";
      $out .= "<td ><input type='text' name='account_serial' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Account Name:</td>";
      $out .= "<td ><input type='text' name='account_name' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Item Code:</td>";
      $out .= "<td ><input type='text' name='item_id' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Upgrades:</td>";
      $out .= "<td>";
      $out .= generate_upgrade_input_html(array ());
      $out .= "</td>";
      $out .= "<tr>";
      $out .= "<td>Item Amount min:</td>";
      $out .= "<td ><input type='text' name='item_amount_min' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Item amount max:</td>";
      $out .= "<td ><input type='text' name='item_amount_max' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Accounts per page:</td>";

      $out .= "<td ><select name='page_size'>";
      $v10 = "";
      if ($page_size == 10)
      {
        $v10 = "selected";
      }
      $out .= "<option value='10'" . $v10 . ">10</option>";
      $v15 = "";
      if ($page_size == 15)
      {
        $v15 = "selected";
      }
      $out .= "<option value='15'" . $v15 . ">15</option>";
      $v25 = "";
      if ($page_size == 25)
      {
        $v25 = "selected";
      }
      $out .= "<option value='25'" . $v25 . ">25</option>";
      $v50 = "";
      if ($page_size == 50)
      {
        $v50 = "selected";
      }
      $out .= "<option value='50'" . $v50 . ">50</option>";
      $v100 = "";
      if ($page_size == 100)
      {
        $v100 = "selected";
      }
      $out .= "<option value='100'" . $v100 . ">100</option>";
      $out .= "</select></td> ";



      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td colspan='2'><input type='hidden' name='action' value='" . $_GET['action'] . "'><input class='ink-button' type='submit' value='Search' name='do_search' style='margin-left: 0px;'/></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";

      $messages = array ();
      if ($do_search != "")
      {
        $out .= "<br/><br/>";

        if ($item_id == "" && $account_serial == 0 && $account_name == "" && $item_slots == 0)
        {
          $messages[] = "Please fill in either the Account Serial, Account Name or Item ID";
        }
        if (count($messages) == 0)
        {
          $bag_numbers = "";
          $i = 0;
          while ($i < 100)
          {
            $bag_numbers .= ", I.K{$i}, I.U{$i}, I.D{$i}";
            ++$i;
          }
          $search_query .= " WHERE ";
          if ($account_name != "")
          {
            $attempt = get_account_serial($account_name);
            $account_serial = $attempt["serial"];
            $messages[] = $attempt["errorMessage"];
            $search_query .= "I.AccountSerial = '{$account_serial}'";
          }
          else if ($account_serial != 0)
          {
            $search_query .= "I.AccountSerial = '{$account_serial}'";
          }
          if ($item_id != "")
          {
            $item_id = str_replace(" ", "", $item_id);
            $item_id = antiject($item_id);

            $attempt = get_item_info_from_code($item_id);
            if ($attempt["error"] == True)
            {
              $messages[] = $attempt["errorMessage"];
            }
            else
            {
              $item_kind = parse_item_code($item_id);
              $item_db_id = $attempt['item']["item_id"];

              $search_query .= "(";
              $item_id_start = ceil(65536 * $item_db_id + ( $item_kind * 256 + 0 ));
              $item_id_end = ceil(65536 * $item_db_id + ( $item_kind * 256 + 255 ));
              $i = 0;
              while ($i < 100)
              {
                if ($i != 0)
                {
                  $search_query .= " OR ";
                }
                if ($item_amount_min != 0)
                {
                  if ($item_amount_max == 0)
                  {
                    $item_amount_max = $item_amount_min;
                  }
                  $append_amount = " AND I.D{$i} >= {$item_amount_min} AND I.D{$i} <= {$item_amount_max}";
                }
                else
                {
                  $append_amount = "";
                }
                $search_query .= "I.K{$i} >= {$item_id_start} AND I.K{$i} <= {$item_id_end}" . $append_amount;
                ++$i;
              }
              $search_query .= ")";
            }
            $enable_itemsearch = true;
          }
          if (count($item_upgrades) > 0)
          {
            $empty = array ();
            $item_ups = 0;
            foreach ($item_upgrades as $index => $talic)
            {
              $temp = intval($talic);
              if ($temp < 0 || $temp == 14 || $temp > 15)
              {
                $messages[] = "Talic " . ($index + 1) . " is not a valid talic.";
              }

              if ($temp > 0 && $temp < 14 && count($empty) > 0)
              {
                $messages[] = "Talic " . ($index + 1) . " cannot be set if all previous talics are not set.";
              }
              else if ($temp > 0 && $temp < 14)
              {
                $item_ups++;
              }

              if ($temp == 15)
              {
                $empty[] = $index;
              }
            }

            if ($item_slots < $item_ups)
            {
              $item_ups = $item_slots;
            }
            if ($item_slots > 7)
            {
              $item_slots = 7;
            }
            if (count($item_upgrades) < 7)
            {
              $i = count($item_upgrades);
              while ($i < 7)
              {
                $item_upgrades[] = 15;
                $i++;
              }
            }

            $item_upgrade = $item_slots . "";
            foreach (array_reverse($item_upgrades) as $index => $talic)
            {
              $item_upgrade .= dechex($talic);
            }
            $item_upgrade = hexdec($item_upgrade);

            if ($account_name != "" || $account_serial != 0 || $enable_itemsearch)
            {
              $search_query .= " AND ";
            }
            $search_query .= "(";

            for ($i = 0; $i < 100; $i++)
            {
              if ($i != 0)
              {
                $search_query .= " OR ";
              }
              if (!$enable_itemsearch)
              {
                $append_k = " AND k{$i} != '-1'";
              }
              else
              {
                $append_k = "";
              }

              $search_query .= "u{$i} = {$item_upgrade}" . $append_k;
            }

            $search_query .= ")";
            $enable_itemsearch = true;
          }

          $item_id = antiject($item_id);

          include( "./core/pagination.php" );

          $sql = "SELECT I.AccountSerial AS Serial{$bag_numbers} FROM  tbl_AccountTrunk AS I {$search_query} ";
          $sql .= "AND I.AccountSerial NOT IN ( SELECT TOP [OFFSET] I.AccountSerial FROM tbl_AccountTrunk AS I ";
          $sql .= "{$search_query} ORDER BY I.AccountSerial DESC) ORDER BY I.AccountSerial DESC";
          $sql_count = "SELECT COUNT(I.AccountSerial) AS Serial FROM tbl_AccountTrunk AS I  {$search_query}";

          $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
          $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

          $pager = new Pagination(DATA, $sql, $sql_count, $url, array (), array (), $page_size, $links_to_show = 11);
          $results = $pager -> get_data();


          $out .= "<table class = 'ink-table hover'>";
          $out .= "<thead>";
          $out .= "<tr>";
          $out .= "<th class='align-left'>Slot</th>";
          $out .= "<th class='align-left'>Item Code</th>";
          $out .= "<th class='align-left'>Item Name</th>";
          $out .= "<th class='align-left'>Amount</th>";
          $out .= "<th class='align-left'>Upgrades</th>";
          $out .= "</tr>";
          $out .= "</thead>";
          $out .= "<tbody>";

          foreach ($results["rows"] as $key => $row)
          {
            $udata = get_user_data($row['Serial']);
            if ($udata['error'])
            {
              $out .= "   <tr>";
              $out .= "       <th class ='thead' colspan='5'>Account Serial:&nbsp;" . $row['Serial'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . "Account Name Missing" . "</th> \n";
              $out .= "   </tr>";
            }
            else
            {
              $out .= "   <tr>";
              $out .= "       <th class ='thead' colspan='5'>Account Serial:&nbsp;" . $row['Serial'] . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Account Name:&nbsp;" . $udata['data']['AccountName'] . "</th> \n";
              $out .= "   </tr>";
            }
            $i = 0;

            for ($i = 0; $i < $num_of_bags; $i++)
            {
              $k_value = $row["K{$i}"];
              $u_value = $row["U{$i}"];

              if (-1 == $k_value)
              {
                $item_id = $k_value;
                $item_code = "-";
                $images = "";
                $item_image = "";
              }
              else
              {
                $item_details = array ();
                $item_details = get_item_details($k_value, $u_value);
                $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);
                $item_code = $item_info["item"]["item_code"];
                $item_id = $item_info["item"]["item_name"];
                $images = $item_details["images"];
                $item_path = glob("./images/items/" . get_image_folder_name($item_details["type"]) . "/(" . $item_info["item"]["item_icon_id"] . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
                if (file_exists(@$item_path[0]))
                {
                  $item_image = "<img id='' class='market_item_image' src='" . $item_path[0] . "' style='height: 40px; width: 40px;'>";
                }
                else
                {
                  $item_image = "";
                }



                if ($item_info["item"]["item_name"] != "")
                {
                  $item_id = str_replace("_", " ", $item_info["item"]["item_name"]);
                }
                else
                {
                  $messages[] = $item_details['errorMessage'];
                }
              }
              if ($do_search != "" && $enable_itemsearch && $item_id != -1)
              {
                if ($item_code == strtolower($_GET['item_id']))
                {
                  $out .= "<tr>";
                  $out .= "<td nowrap>" . $i . "</td>";
                  $out .= "<td nowrap>" . $item_code . "</td>";
                  $out .= "<td nowrap>" . $item_image . " " . $item_id . "</td>";
                  $out .= "<td nowrap>" . $row["D{$i}"] . "</td>";
                  $out .= "<td nowrap>" . $images . "</td>";
                  $out .= "</tr>";
                }
                else
                {
                  if ($u_value == $item_upgrade && $item_slots > 0)
                  {
                    $out .= "<tr>";
                    $out .= "<td nowrap>" . $i . "</td>";
                    $out .= "<td nowrap>" . $item_code . "</td>";
                    $out .= "<td style='' nowrap>" . $item_image . " " . $item_id . "</td>";
                    $out .= "<td nowrap>" . $row["D{$i}"] . "</td>";
                    $out .= "<td style='' nowrap>" . $images . "</td>";
                    $out .= "</tr>";
                  }
                }
              }
              else if ($item_id != -1)
              {
                $bgcolor = "";
                $out .= "<tr>";
                $out .= "<td style='" . $bgcolor . "' nowrap>" . $i . "</td>";
                $out .= "<td style='" . $bgcolor . "' nowrap>" . $item_code . "</td>";
                $out .= "<td style='" . $bgcolor . "' nowrap>" . $item_image . " " . $item_id . "</td>";
                $out .= "<td style='" . $bgcolor . "' nowrap>" . $row["D{$i}"] . "</td>";
                $out .= "<td style='" . $bgcolor . "' nowrap>" . $images . "</td>";
                $out .= "</tr>";
              }
            }
          }
          if ($enable_itemsearch == true)
          {
            $out .= "</tbody>";
            $out .= "<tfoot>";
            if (count($results["rows"]) <= 0)
            {
              $out .= "<tr>";
              $out .= "<td  colspan='9' style='text-align: center; font-weight: bold;'>No items  found.</td>";
              $out .= "</tr>";
            }
            else
            {
              $out .= "<tr>";
              $out .= "<td  colspan='9' style='text-align: center; font-weight: bold;'>" . $pager -> renderFullNav() . "</td>";
              $out .= "</tr>";
            }
            $out .= "</tfoot>";
          }
          $out .= "</table>";
          if (!isset($item_code))
          {
            $item_code = "";
          }
          gamecp_log(0, $userdata -> username, "ADMIN - BANK SEARCH - Searched for: {$account_name} or {$account_serial} or {$item_code}", 1);
        }
        if (count($messages) > 0)
        {
          $out .= get_notification_html($messages, ERROR);
        }
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
