<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Administration";
  $moduleLabel = "Newsletter";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    include_once( "./includes/class.phpmailer.php" );
    $page = isset($_REQUEST["page"]) ? $_REQUEST["page"] : "";

    if ($page == "")
    {
      $out .= "<form class=\"ink-form\" method=\"post\">";
      $out .= "<table class=\"newsleter_layout\">";
      $out .= "<tr>";
      $out .= "<td>Subject: </td>";
      $out .= "<td><input type=\"text\" name=\"subject\" style=\"width: 250px;\"></input></td>";
      $out .= "</tr><tr>";
      $out .= "<td>Email Contents: </td>";
      $out .= "<td><textarea id=\"contents\" name=\"contents\" style=\"height: 350px;\"></textarea></td>";
      $out .= "</tr><tr>";
      $out .= "<td>Email Contents Plain Text: </td>";
      $out .= "<td><textarea name=\"plain_contents\" style=\"height: 250px; width: 100%;\"></textarea></td>";
      $out .= "</tr><tr>";
      $out .= "<td colspan=2>";
      $out .= "<input type=\"hidden\" name=\"page\" value=\"send_mail\" />";
      $out .= "<input class=\"ink-button\" type=\"submit\" value=\"Send\" name=\"submit\" style=\"margin-left: 0px;\"/>";
      $out .= "</td>";
      $out .= "</tr></table>";
      $out .= "</form>";

      $out .= "<script>tinymce.init({selector:'#contents',plugins: [\"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker\",";
      $out .= "\"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking\",";
      $out .= "\"save table contextmenu directionality emoticons template paste textcolor colorpicker\" ]});</script>";
    }
    else if ($page == "send_mail")
    {
      $emails = get_user_emails();
      $messages = array ();
      if ($emails["error"] == True)
      {
        $out .= get_notification_html($emails["errorMessage"], ERROR);
      }
      else
      {
        $path = explode("/", $_SERVER['REQUEST_URI']);
        unset($path[count($path) - 1]);
        $path = implode("/", $path);
        $unsub_link = "http://" . $_SERVER["HTTP_HOST"] . $path . "/" . "unsubscribe.php?email=[USEREMAIL]&key=[USERKEY]";
        $email_header = "<html><head></head><body>";
        $email_footer = "<br/>";
        $email_footer .= "<center><a href=\"" . $unsub_link . "\">Click here to unsubscribe</a></center>";
        $email_footer .= "</body></html>";
        $emails = $emails["emails"];
        $contents = isset($_POST["contents"]) ? $_POST["contents"] : "";
        $plain_contents = isset($_POST["plain_contents"]) ? $_POST["plain_contents"] : "";
        $subject = isset($_POST["subject"]) ? $_POST["subject"] : "";
        $plain_contents .= "\nLink to unsubscribe: " . $unsub_link;
        $contents = $email_header . $contents . $email_footer;

        if ($subject != "" && $contents != "")
        {
          $sendMail = sendBulkEmail(@$emails, @$subject, @$contents, @$plain_contents);
          if (count($sendMail) > 0)
          {
            $success = false;
            $messages[] = translation("Attempt to send a newsletter has failed:");
            foreach ($sendMail as $key => $email_add)
            {
              $messages[] = $email_add;
            }
          }
          else
          {
            $out .= get_notification_html("Newsletter Sent", SUCCESS);
          }
        }
      }

      if (count($messages) > 0)
      {
        $out .= get_notification_html($messages, ERROR);
      }
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
