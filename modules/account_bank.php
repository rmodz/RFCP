<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Edit";
  $moduleLabel = "Account Bank";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{

  /**
   * @author Mauro Tamm   @ mauro.tamm@gmail.com
   * @author Agony @ agony@nxtdeveloper.com
   * @copyright 2014 http://nxtdeveloper.com/
   * @ver 4.0
   */
  if ($userdata -> has_permission($action))
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $search = isset($_GET['search']) ? $_GET['search'] : "";
    $delete = isset($_GET['delete']) ? $_GET['delete'] : "";
    $account_serial = isset($_GET['account_serial']) && ctype_digit($_GET['account_serial']) ? intval($_GET['account_serial']) : 0;
    $account_name = isset($_GET['account_name']) ? $_GET['account_name'] : "";
    $search_query = "";
    if (empty($page))
    {
      $out .= "<form class='ink-form' method='get' action='" . $script_name . "?action=" . $_GET['action'] . "'>";
      $out .= "<table class='ink-form' cellpadding='3' cellspacing='1' border='0' width='100%' style='font-size: 12pt;'>";
      $out .= "<tr>";
      $out .= "<td>Account Serial:</td>";
      $out .= "<td ><input type='text' name='account_serial' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Account Name:</td>";
      $out .= "<td ><input type='text' name='account_name' size='40'/></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td colspan='2'><input type='hidden' name='action' value='" . $_GET['action'] . "'><input class='ink-button' type='submit' value='Search' name='search' style='margin-left: 0px;'/></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";

      $messages = array ();
      if ($delete != "")
      {
        $dbcon = connectdb(DATA);
        $slot = isset($_GET['slot']) ? intval($_GET['slot']) : "";
        $serial = isset($_GET['serial']) ? $_GET['serial'] : "";
        if (!isset($_GET['ext']))
        {
          $sql = "SELECT k{$slot}, u{$slot}, d{$slot}, r{$slot} FROM tbl_AccountTrunk WHERE AccountSerial = ?";
          $results = sqlsrv_query($dbcon, $sql, array ($serial));
          $row = sqlsrv_fetch_array($results);
          $k_value = $row["k{$slot}"];
          $u_value = $row["u{$slot}"];
          $d_value = $row["d{$slot}"];
          $r_value = $row["r{$slot}"];

          $sql = "UPDATE tbl_AccountTrunk SET K{$slot} = '-1',U{$slot} = '-1',D{$slot} = '0', R{$slot} = '255', S{$slot} = '0', T{$slot} = '-1' WHERE AccountSerial = ?";
          $results = sqlsrv_query($dbcon, $sql, array ($serial));

          item_delete_log($serial, -1, $k_value, $u_value, $d_value, $r_value, $userdata -> serial);
          gamecp_log(0, $userdata -> username, "ADMIN - ACCOUNT BANK - Deleted slot {$slot} for account {$account_name}", 3);
        }
        elseif (isset($_GET['ext']) && $_GET['ext'] == true)
        {
          $sql = "SELECT k{$slot}, u{$slot}, d{$slot}, r{$slot} FROM tbl_AccountTrunk_Extend WHERE AccountSerial = ?";
          $results = sqlsrv_query($dbcon, $sql, array ($serial));
          $row = sqlsrv_fetch_array($results);
          $k_value = $row["k{$slot}"];
          $u_value = $row["u{$slot}"];
          $d_value = $row["d{$slot}"];
          $r_value = $row["r{$slot}"];

          $sql = "UPDATE tbl_AccountTrunk_Extend SET K{$slot} = '-1',U{$slot} = '-1',D{$slot} = '0', R{$slot} = '255', S{$slot} = '0', T{$slot} = '-1' WHERE AccountSerial = ?";
          $results = sqlsrv_query($dbcon, $sql, array ($serial));

          gamecp_log(0, $userdata -> username, "ADMIN - ACCOUNT BANK - Deleted slot extended {$slot} for account {$account_name}", 3);
        }
      }
      if ($search != "")
      {
        $out .= "<br/><br/>";

        if ($account_serial == 0 && $account_name == "")
        {
          $messages[] = "Please fill in either the Account Serial or Account Name";
        }
        if (count($messages) == 0)
        {
          $bag_numbers = "";
          $ext_bag_numbers = "";
          if (EXT_STORAGE === TRUE)
          {
            $i = 0;
            while ($i < 40)
            {
              $ext_bag_numbers .= ", SE.K{$i} AS EK{$i}, SE.U{$i} AS EU{$i}, SE.D{$i} AS ED{$i}";
              ++$i;
            }
          }
          $i = 0;
          while ($i < 100)
          {
            $bag_numbers .= ", S.K{$i}, S.U{$i}, S.D{$i}";
            ++$i;
          }
          $search_query .= " WHERE ";
          if ($account_name != "")
          {
            $attempt = get_account_serial($account_name);
            $account_serial = $attempt["serial"];
            if ($attempt["error"] == True)
            {
              $messages[] = $attempt["errorMessage"];
              $search_query .= "S.AccountSerial = '{$account_serial}'";
            }
          }
          elseif ($account_serial != 0)
          {
            $search_query .= "S.AccountSerial = '{$account_serial}'";
          }
          $sql = "";
          if (EXT_STORAGE === FALSE)
          {
            $sql .= "SELECT AccountSerial {$bag_numbers} FROM tbl_AccountTrunk {$search_query} ";
            $sql .= " ORDER BY AccountSerial DESC";
          }
          elseif (EXT_STORAGE === TRUE)
          {
            $sql .= "SELECT S.AccountSerial, SE.AccountSerial {$bag_numbers}{$ext_bag_numbers} FROM  tbl_AccountTrunk AS S INNER JOIN tbl_AccountTrunk_Extend AS SE ON S.AccountSerial = SE.AccountSerial {$search_query} ";
            $sql .= " ORDER BY S.AccountSerial DESC";
          }

          $out .= "<table class = 'ink-table hover'>";
          $out .= "<thead>";
          $out .= "<tr>";
          $out .= "<th class='align-left'>Slot</th>";
          $out .= "<th class='align-left'>Item Code</th>";
          $out .= "<th class='align-left'>Item Name</th>";
          $out .= "<th class='align-left'>Amount</th>";
          $out .= "<th class='align-left'>Upgrades</th>";
          $out .= "<th class='align-left'>Delete</th>";
          $out .= "</tr>";
          $out .= "</thead>";
          $out .= "<tbody>";

          $dbcon = connectdb(DATA);
          $results = sqlsrv_query($dbcon, $sql);

          while ($row = sqlsrv_fetch_array($results, SQLSRV_FETCH_ASSOC))
          {
            for ($i = 0; $i < 100; $i++)
            {
              $k_value = $row["K{$i}"];
              $u_value = $row["U{$i}"];

              if (-1 == $k_value)
              {
                $item_id = $k_value;
                $item_code = "-";
                $images = "";
                $item_image = "";
              }
              else
              {
                $item_details = array ();
                $item_details = get_item_details($k_value, $u_value);
                $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);
                $item_code = $item_info["item"]["item_code"];
                $item_id = $item_info["item"]["item_name"];
                $images = $item_details["images"];
                $item_path = glob("./images/items/" . get_image_folder_name($item_details["type"]) . "/(" . $item_info["item"]["item_icon_id"] . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
                if (file_exists(@$item_path[0]))
                {
                  $item_image = "<img id='' class='market_item_image' src='" . $item_path[0] . "' style='height: 40px; width: 40px;'>";
                }
                else
                {
                  $item_image = "";
                }
              }
              if ($search != "" && $item_code != "-")
              {
                $out .= "<tr>";
                $out .= "<td nowrap>" . $i . "</td>";
                $out .= "<td nowrap>" . $item_code . "</td>";
                $out .= "<td nowrap>" . $item_image . " " . $item_id . "</td>";
                $out .= "<td nowrap>" . $row["D{$i}"] . "</td>";
                $out .= "<td nowrap>" . $images . "</td>";
                $out .= "<td><a class='icon_link " . BASE_COLOR . " 'href='" . $script_name . "?action=" . $_GET['action'] . "&account_name=" . $account_name . "&account_serial=" . $account_serial . "&search=" . $search . "&delete=delete&slot=" . $i . "&serial=" . $row['AccountSerial'] . "'><i class='icon-trash icon-large' title='Delete Item'></i></a></td>";
                $out .= "</tr>";
              }
              else if ($item_code == "-")
              {
                $bgcolor = "color: blue;";
                $out .= "<tr>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $i . "</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $item_code . "</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>Empty Slot</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row["D{$i}"] . "</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $images . "</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>-</td>";
                $out .= "</tr>";
              }
            }
            if (EXT_STORAGE === TRUE)
            {
              for ($i = 0; $i < 40; $i++)
              {
                $k_value = $row["EK{$i}"];
                $u_value = $row["EU{$i}"];

                if (-1 == $k_value)
                {
                  $item_id = $k_value;
                  $item_code = "-";
                  $images = "";
                  $item_image = "";
                }
                else
                {
                  $item_details = array ();
                  $item_details = get_item_details($k_value, $u_value);
                  $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);
                  $item_code = $item_info["item"]["item_code"];
                  $item_id = $item_info["item"]["item_name"];
                  $images = $item_details["images"];
                  $item_path = glob("./images/items/" . get_image_folder_name($item_details["type"]) . "/(" . $item_info["item"]["item_icon_id"] . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
                  if (file_exists(@$item_path[0]))
                  {
                    $item_image = "<img id='' class='market_item_image' src='" . $item_path[0] . "' style='height: 40px; width: 40px;'>";
                  }
                  else
                  {
                    $item_image = "";
                  }
                }
                if ($search != "" && $item_code != "-")
                {
                  $out .= "<tr>";
                  $out .= "<td nowrap>" . $i . " EXT</td>";
                  $out .= "<td nowrap>" . $item_code . "</td>";
                  $out .= "<td nowrap>" . $item_image . " " . $item_id . "</td>";
                  $out .= "<td nowrap>" . $row["ED{$i}"] . "</td>";
                  $out .= "<td nowrap>" . $images . "</td>";
                  $out .= "<td><a class='icon_link " . BASE_COLOR . " 'href='" . $script_name . "?action=" . $_GET['action'] . "&account_name=" . $account_name . "&account_serial=" . $account_serial . "&search=" . $search . "&delete=delete&slot=" . $i . "&serial=" . $row['AccountSerial'] . "&ext=true'><i class='icon-trash icon-large' title='Delete Item'></i></a></td>";
                  $out .= "</tr>";
                }
                elseif ($item_code == "-")
                {
                  $bgcolor = "color: blue;";
                  $out .= "<tr>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $i . " EXT</td>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $item_code . "</td>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>Empty Slot</td>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row["ED{$i}"] . "</td>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $images . "</td>";
                  $out .= "<td style=\"" . $bgcolor . "\" nowrap>-</td>";
                  $out .= "</tr>";
                }
              }
            }
          }
          $out .= "</table>";
          if (!isset($item_code))
          {
            $item_code = "";
          }
          gamecp_log(0, $userdata -> username, "ADMIN - ACCOUNT BANK - Searched for: {$account_name} or {$account_serial}", 1);
        }
        if (count($messages) > 0)
        {
          $out .= get_notification_html($messages, ERROR);
        }
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
