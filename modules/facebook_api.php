<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "HIDDEN";
  $moduleLabel = "facebook api";
  $permission = "restricted";
  return;
}
$out_type = OUTPUT_JSON;
if ($this_script == $script_name)
{
  $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : null;
  $update = false;
  if ($type == "fbLink")
  {
    $userId = isset($_REQUEST["userId"]) ? $_REQUEST["userId"] : null;
    $userdata -> facebookUserId = $userId;
    if ($userId)
    {
      $userSql = "UPDATE " . TABLE_LUACCOUNT . " SET FacebookUserId = ? WHERE id = CONVERT(binary,?)";
      $params = array ($userId, $userdata -> username);
      $update = sqlsrv_query(connectdb(USER), $userSql, $params);
    }
  }
  if ($type == "fbUnlink")
  {
    $userdata -> facebookUserId = null;
    $userSql = "UPDATE " . TABLE_LUACCOUNT . " SET FacebookUserId = ? WHERE id = CONVERT(binary,?)";
    $params = array (null, $userdata -> username);
    $update = sqlsrv_query(connectdb(USER), $userSql, $params);
  }
  return $update;
}
