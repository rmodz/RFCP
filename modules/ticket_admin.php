<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Support";
  $moduleLabel = "Ticket Admin";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  /**
   * @author Mauro Tamm   @ mauro.tamm@gmail.com
   * @author Agony @ agony@nxtdeveloper.com
   * @copyright 2014 http://nxtdeveloper.com/
   * @ver 1.0
   */
  if ($userdata -> has_permission($action))
  {

    function ticketclose($id)
    {
      $sql = "UPDATE  gamecp_ticket_entry SET status = 2 WHERE id = " . $id . "";
      $tdata = sqlsrv_query(connectdb(RFCP), $sql);
      if ($tdata === false)
      {
        return get_notification_html(array ('Error Updating Status'), ERROR);
      }
    }

    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $sort = isset($_GET['sort']) ? $_GET['sort'] : "id DESC";
    $account = isset($_POST['account']) ? antiject($_POST['account']) : "";
    $squery = "";
    $squery2 = "";
    $category = isset($_GET['type']) ? antiject($_GET['type']) : "";
    $messages = array ();
    $permission = get_ticket_permission($userdata -> serial);
    $permission = $permission["permission"];
    if (empty($page))
    {
      if ($sort == "category")
      {
        $sort = "type DESC";
      }
      if ($sort == "status")
      {
        $sort = "status ASC";
      }
      if ($sort == "id")
      {
        $sort = "id ASC";
      }

      if ($category != "" && $category != "all")
      {
        $squery = "type = '" . $category . "'";
        $squery2 = " AND";
      }

      if ($account != "")
      {
        $squery = "account LIKE '%" . $account . "%'";
        $squery2 = " AND";
      }

      include( "./core/pagination.php" );
      $accname = $userdata -> username;

      $sql = "SELECT id, date, type, account, title, status, DATEDIFF(m, date, getdate()) AS month, DATEDIFF(d, date, getdate()) AS day, DATEDIFF(hh, date, getdate()) AS hour, DATEDIFF(n, date, getdate()) AS minute ";
      $sql .= "FROM gamecp_ticket_entry WHERE permission <= {$permission} AND {$squery}{$squery2} id NOT IN (";
      $sql .= "SELECT TOP [OFFSET] id ";
      $sql .= "FROM gamecp_ticket_entry WHERE {$squery}{$squery2} permission <= {$permission} ORDER BY " . $sort . " ) ORDER BY " . $sort . "";

      $sql_count = "SELECT COUNT(id)  FROM gamecp_ticket_entry WHERE {$squery}{$squery2} permission <= {$permission}";

      $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
      $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

      $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 15, $links_to_show = 11);
      $results = $pager -> get_data();

      if ($results === false)
      {
        $out .= get_notification_html(array ('Error getting Ticket Data'), ERROR);
      }
      else
      {
        if (!$results === false)
        {
          $categories = get_ticket_category_list();
          $cat = $categories['categories'];
          $csize = sizeof($cat);

          $sorturl = $script_name . "?action=" . $_GET['action'] . "&type=";
          $url = $script_name . "?action=" . $_GET['action'];
          $out .= "<table class = 'ink-table hover'>";
          $out .= "<thead>";
          $out .= "<tr>";
          $out .= "<th colspan = 1 style = 'border-color:#FFFFFF;'></th>";
          $out .= "<th class='align-left' colspan = 2 style = 'border-color:#FFFFFF;'><form id ='input' method = 'post' action ='$url'><input type='text' name='account' style = 'width:200px; padding-left: 5px' placeholder='Press enter to search accounts' value = '" . $account . "'/></form></th>";
          $out .= "<th colspan = 3 class='align-left' style = 'border-color:#FFFFFF;'><form><select style='width:220px' name='type' onchange=\"navigation('$sorturl' + this.options[this.selectedIndex].value);\">";
          $sel = "";

          if ($category == "all")
          {
            $sel = "selected";
          }

          $out .= "<option value='all'" . $sel . ">All</option>";

          for ($i = 0; $i < $csize; $i++)
          {
            if ($category == $cat[$i]['id'])
            {
              $sel = "selected";
            }
            $out .= "<option value='" . $cat[$i]['id'] . "'" . $sel . ">" . $cat[$i]['category'] . "</option>";
          }

          $out .= "</select></form></th>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<th class='align-left'  style='width:100px'><a href='" . $script_name . "?action=" . $_GET['action'] . "&sort=id'>ID</th>";
          $out .= "<th class='align-left'  style='width:100px'>ACCOUNT</th>";
          $out .= "<th class='align-left'>SUBJECT</th>";
          $out .= "<th class='align-left'  style='width:220px'><a href='" . $script_name . "?action=" . $_GET['action'] . "&sort=category'>CATEGORY</th>";
          $out .= "<th class='align-left' style='width:150px'>LAST ACTIVITY</th>";
          $out .= "<th class='align-left'  style='width:100px'><a href='" . $script_name . "?action=" . $_GET['action'] . "&sort=status'>STATUS</th>";
          if ($userdata -> is_superadmin())
          {
            $out .= "<th class='align-left'  style='width:50px'>TOOLS</th>";
          }

          $out .= "</tr>";
          $out .= "</thead>";

          foreach ($results["rows"] as $key => $row)
          {
            $rowurl = $script_name . "?action=show_ticket_admin&id=" . $row['id'] . "&account=" . $row['account'];
            $ticketid = $row['id'];

            if ($row['month'] > 0)
            {
              $daysago = $row['month'] . " Months Ago";
              $out .= ticketclose($ticketid);
            }
            else if ($row['day'] != 0)
            {
              $daysago = $row['day'] . " Days Ago";
            }
            else if ($row['hour'] != 0)
            {
              $daysago = $row['hour'] . " Hours Ago";
            }
            else if ($row['minute'] != 0)
            {
              $daysago = $row['minute'] . " Minutes Ago";
            }
            else
            {
              $daysago = "1 Minutes Ago";
            }

            $cat_type = get_ticket_category($row['type']);
            if ($cat_type['permission'] <= $permission)
            {
              $out .= "<tr>";
              $out .= "<td onclick=\"navigation('$rowurl');\">" . $ticketid . "</td>";
              $out .= "<td><a href='" . $script_name . "?action=account_search&account_name=" . $row['account'] . "&do_search=Search' target='_blank'>" . $row['account'] . "</td>";
              $out .= "<td onclick=\"navigation('$rowurl');\">" . filter_string_for_html($row['title']) . "</td>";
              if ($cat_type['category'] != "")
              {
                $out .= "<td onclick=\"navigation('$rowurl');\">" . $cat_type['category'] . "</td>";
              }
              else
              {
                $out .= "<td onclick=\"navigation('$rowurl');\"><b>Missing Category</b></td>";
              }
              $out .= "<td onclick=\"navigation('$rowurl');\">" . $daysago . "</td>";
              $out .= "<td>";
              $out .= "<select name='status' onchange=\"changeStatus(this.options[this.selectedIndex].value,'$ticketid');\">";
              $s0 = "";
              if ($row['status'] == 0)
              {
                $s0 = "selected";
              }
              $out .= "<option value='0'" . $s0 . ">" . status(0) . "</option>";
              $s1 = "";
              if ($row['status'] == 1)
              {
                $s1 = "selected";
              }
              $out .= "<option value='1'" . $s1 . ">" . status(1) . "</option>";
              $s2 = "";
              if ($row['status'] == 2)
              {
                $s2 = "selected";
              }
              $out .= "<option value='2'" . $s2 . ">" . status(2) . "</option>";
              $out .= "</select>";
              $out .= "</td>";
              if ($userdata -> is_superadmin())
              {
                $out .= "<td><a class='icon_link " . BASE_COLOR . " 'href='" . $_SERVER['REQUEST_URI'] . "&page=delete&id=" . $ticketid . "&account=" . $row['account'] . "'><i class='icon-trash icon-large' title='Delete Ticket'></i></a></td>";
              }

              $out .= "</tr>";
            }
          }
          if (count($results["rows"]) <= 0)
          {
            $out .= "<tr>";
            $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">No tickets found.</td>";
            $out .= "</tr>";
          }
          else
          {
            $out .= "<tr>";
            $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
            $out .= "</tr>";
          }
          $out .= "</tbody>";
          $out .= "</table>";
        }
        else
        {
          $out .= get_notification_html(array ('No Tickets found'), ERROR);
        }
      }

      $out .= "<script type=\"text/javascript\">" . "\n";
      $out .= "$( \"#account\" ).keyup(function() {" . "\n";
      $out .= "	var txtinput = $('#account').val();" . "\n";
      $out .= "});" . "\n";
      $out .= "</script>" . "\n";
    }
    else if ($page == "delete")
    {
      $esql = "DELETE FROM  gamecp_ticket_entry WHERE id = ?";
      $dsql = "DELETE FROM  gamecp_ticket_data WHERE entryid = ?";
      $param = array ($_GET['id']);
      $tentry = sqlsrv_query(connectdb(RFCP), $esql, $param);
      if ($tentry === false)
      {
        $out .= get_notification_html(array ('Error Deleting Ticket Entry'), ERROR);
      }
      $tdata = sqlsrv_query(connectdb(RFCP), $dsql, $param);
      if ($tdata === false)
      {
        $out .= get_notification_html(array ('Error Deleting Ticket Entry'), ERROR);
      }
      gamecp_log(2, $userdata -> username, "SUPER ADMIN - CONFIG - Deleted Ticket ID: " . $_GET['id'] . " and Account: " . $_GET['account'] . "", 1);
      $out .= get_notification_html(array ('Ticket Deleted'), SUCCESS);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
