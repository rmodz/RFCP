<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "HIDDEN";
  $moduleLabel = "vote";
  $permission = "all";
  return;
}

$out_type = OUTPUT_JSON;
if ($this_script == $script_name)
{
  $vote_id = isset($_POST['vote_id']) && ctype_digit($_POST['vote_id']) ? intval($_POST['vote_id']) : "";
  $account_name = isset($_POST['vote_account']) ? antiject($_POST['vote_account']) : "";
  $do = isset($_POST['do']) ? $_POST['do'] : "";
  $attempt = get_vote_sites();
  $sites = $attempt["rows"];
  $out_json["error"] = False;
  $out_json["errorMessage"] = False;

  if ($do == "claim_credits")
  {
    $max_gp = isset($config['vote_max_gp']) ? $config['vote_max_gp'] : 4;
    $min_gp = isset($config['vote_min_gp']) ? $config['vote_min_gp'] : 1;
    if ($max_gp <= 0)
    {
      $max_gp = 4;
    }
    if ($min_gp <= 0)
    {
      $min_gp = 1;
    }
    if ($max_gp < $min_gp)
    {
      $max_gp = $min_gp;
    }
    if ($max_gp < 1 || $min_gp < 1)
    {
      $gp_increase = rand($min_gp, $max_gp);
    }
    else
    {
      $gp_increase = rand($min_gp, $max_gp);
    }

    $out_json["credits"] = $gp_increase;

    if (!isset($_SESSION['account_serial']) || !isset($_SESSION['vote_id']) || !isset($_SESSION['start_time']))
    {
      $out_json["error"] = True;
      $out_json["errorMessage"] = "Session Error!";
    }
    else if (time() - $_SESSION['start_time'] < 10)
    {
      $out_json["error"] = True;
      $out_json["errorMessage"] = "Please do not try to cheat the system!";
    }
    else
    {
      if (!isset($_SESSION['account_serial']) || $_SESSION['account_serial'] == "")
      {
        $out_json["error"] = True;
        $out_json["errorMessage"] = "User not found.";
      }
      else
      {
        $log_info = get_last_vote($_SESSION['account_serial'], $_SESSION['vote_id']);
        if ($log_info["error"] == True)
        {
          $out_json["error"] = True;
          $out_json["errorMessage"] = $log_info["errorMessage"];
        }
        else
        {
          $last_vote_time = $log_info["vote_log"]["log_time"];
          $time_check = time() - $last_vote_time;
          if (($time_check < $sites[$_SESSION['vote_id']]["vote_reset_time"]))
          {
            $now_diff = $sites[$_SESSION['vote_id']]["vote_reset_time"] - $time_check;
            $time_arr = array ("hours" => floor($now_diff / 3600), "minutes" => floor(($now_diff / 60) % 60),
              "seconds" => ($now_diff % 60));
            $time_string = sprintf('%02d', $time_arr['hours']) . ":" . sprintf('%02d', $time_arr['minutes']) . ":" . sprintf('%02d', $time_arr['seconds']);
            $out_json["error"] = True;
            $out_json["errorMessage"] = "Sorry, you have not waited long enough, " . $time_string . " until you can vote again.";
          }
          else
          {
            $add_credits_attempt = add_user_credits($_SESSION['account_serial'], $gp_increase);
            if ($add_credits_attempt["error"] == True)
            {
              $out_json["error"] = True;
              $out_json["errorMessage"] = "Error while trying to add credits!";
            }
            else
            {
              $total_credits = get_user_credits($_SESSION['account_serial']);
              vote_log($_SESSION['vote_id'], $_SESSION['account_serial'], $ip, $gp_increase, $total_credits["credits"]);
            }
            session_destroy();
          }
        }
      }
    }
  }
  else
  {
    if (!preg_match(REGEX_USERNAME, $account_name) || strlen($account_name) < 4 || 12 < strlen($account_name))
    {
      $out_json["error"] = True;
      $out_json["errorMessage"] = "Invalid username provided.";
    }
    else if (!array_key_exists($vote_id, $sites))
    {
      $out_json["error"] = True;
      $out_json["errorMessage"] = "Invalid vote site id.";
    }
    else
    {
      $attempt = get_account_serial($account_name);
      if ($attempt["error"] == True || $attempt['serial'] == -1)
      {
        $out_json["error"] = True;
        $out_json["errorMessage"] = "User not found.";
      }
      else
      {
        $log_info = get_last_vote($attempt["serial"], $vote_id);
        if ($log_info["error"] == True)
        {
          $out_json["error"] = True;
          $out_json["errorMessage"] = $log_info["errorMessage"];
        }
        else
        {
          $last_vote_time = $log_info["vote_log"]["log_time"];
          $time_check = time() - $last_vote_time;
          if (($time_check < $sites[$vote_id]["vote_reset_time"]))
          {
            $now_diff = $sites[$vote_id]["vote_reset_time"] - $time_check;
            $time_arr = array ("hours" => floor($now_diff / 3600), "minutes" => floor(($now_diff / 60) % 60),
              "seconds" => ($now_diff % 60));
            $time_string = sprintf('%02d', $time_arr['hours']) . ":" . sprintf('%02d', $time_arr['minutes']) . ":" . sprintf('%02d', $time_arr['seconds']);
            $out_json["error"] = True;
            $out_json["errorMessage"] = "Sorry, you have not waited long enough, " . $time_string . " until you can vote again.";
          }
        }
      }

      if ($out_json["error"] == False)
      {
        $_SESSION['account_serial'] = $attempt["serial"];
        $_SESSION['vote_id'] = $vote_id;
        $_SESSION['start_time'] = time();
        $out_json["time_to_vote"] = 10;
      }
    }
  }
}