<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "User";
  $moduleLabel = "My Tickets";
  $permission = "player";
  return;
}
if ($this_script == $script_name)
{
  /**
   * @author Mauro Tamm   @ mauro.tamm@gmail.com
   * @author Agony @ agony@nxtdeveloper.com
   * @copyright 2014 http://nxtdeveloper.com/
   * @ver 1.0
   */
  ?>
  <script type="text/javascript">
    function navigation(Url)
    {
      document.location.href = Url;
    }
  </script>
  <?php

  if ($userdata -> loggedin == True)
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $messages = array ();
    if (empty($page))
    {
      include( "./core/pagination.php" );
      $accname = $userdata -> username;
      $sql = "SELECT id, date, type, account, title, status, DATEDIFF(m, date, getdate()) AS month, DATEDIFF(d, date, getdate()) AS day, DATEDIFF(hh, date, getdate()) AS hour, DATEDIFF(n, date, getdate()) AS minute ";
      $sql .= "FROM gamecp_ticket_entry WHERE account = '{$accname}' AND id NOT IN (";
      $sql .= "SELECT TOP [OFFSET] id ";
      $sql .= "FROM gamecp_ticket_entry WHERE account = '{$accname}' ORDER BY id DESC) ORDER BY id DESC";

      $sql_count = "SELECT COUNT(id)  FROM gamecp_ticket_entry WHERE account = '{$accname}'";

      $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
      $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

      $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 15, $links_to_show = 11);
      $results = $pager -> get_data();

      if ($results === false)
      {
        $out .= get_notification_html(array ('Error getting Ticket Data'), ERROR);
      }
      else
      {
        $out .= "<form class='ink-form' method='post' action='" . $script_name . "?action=new_ticket'>";
        $out .= "<div style = 'width: 100%; height: 30px; margin-top: 10px; text-align: left;'>";
        $out .= "<input class='ink-button' style ='' type='submit' value='New Ticket'/>";
        $out .= "</div>";
        $out .= "</form>";

        if (!$results === false)
        {
          $out .= "<table class = 'ink-table hover'>";
          $out .= "<thead>";
          $out .= "<tr>";
          $out .= "<th class='align-left'  style='width:100px'>ID</th>";
          $out .= "<th class='align-left'>SUBJECT</th>";
          $out .= "<th class='align-left' style='width:150px'>LAST ACTIVITY</th>";
          $out .= "<th class='align-left' style='width:100px'>STATUS</th>";
          $out .= "</tr>";
          $out .= "</thead>";

          foreach ($results["rows"] as $key => $row)
          {
            if ($row['month'] != 0)
            {
              $daysago = $row['month'] . " Months Ago";
            }
            else if ($row['day'] != 0)
            {
              $daysago = $row['day'] . " Days Ago";
            }
            else if ($row['hour'] != 0)
            {
              $daysago = $row['hour'] . " Hours Ago";
            }
            else if ($row['minute'] != 0)
            {
              $daysago = $row['minute'] . " Minutes Ago";
            }
            else
            {
              $daysago = "1 Minutes Ago";
            }

            $rowurl = $script_name . "?action=show_ticket&id=" . $row['id'];

            $out .= "<tr onclick=\"navigation('$rowurl');\">";
            $out .= "<td>" . $row['id'] . "</td>";
            $out .= "<td>" . filter_string_for_html($row['title']) . "</td>";
            $out .= "<td>" . $daysago . "</td>";
            $out .= "<td>" . status($row['status']) . "</td>";
            $out .= "</tr>";
          }

          if (count($results["rows"]) <= 0)
          {
            $out .= "<tr>";
            $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">No Tickets found.</td>";
            $out .= "</tr>";
          }
          else
          {
            $out .= "<tr>";
            $out .= "<td  colspan=\"9\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
            $out .= "</tr>";
          }
          $out .= "</tbody>";
          $out .= "</table>";
        }
        else
        {
          $out .= get_notification_html(array ('No Tickets found'), ERROR);
        }
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
