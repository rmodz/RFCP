<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Logs";
  $moduleLabel = "Namechange Logs - Zone";
  $permission = "restricted";
  return;
}

if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    include( "./core/pagination.php" );

    $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
    $out .= "<tr>";
    $out .= "<th class='align-left'>ID</th>";
    $out .= "<th class='align-left'>Char ID</th>";
    $out .= "<th class='align-left'>Old Name</th>";
    $out .= "<th class='align-left'>New Name</th>";
    $out .= "</tr>";

    $sql = "SELECT Idx, Serial, OldName, NewName ";
    $sql .= "FROM tbl_RenamePotion_Log ";
    $sql .= "WHERE Idx NOT IN ( SELECT TOP [OFFSET] Idx FROM tbl_RenamePotion_Log ";
    $sql .= "ORDER BY Idx DESC) ORDER BY Idx DESC";
    $sql_count = "Select COUNT(Idx) FROM tbl_RenamePotion_Log";

    $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
    $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

    $pager = new Pagination(DATA, $sql, $sql_count, $url, array (), array (), $page_size = 30, $links_to_show = 10);
    $results = $pager -> get_data();

    foreach ($results["rows"] as $key => $row)
    {
      $out .= "<tr>";
      $out .= "<td nowrap>" . $row['Idx'] . "</td>";
      $out .= "<td nowrap>" . $row['Serial'] . "</td>";
      $out .= "<td nowrap>" . $row['OldName'] . "</td>";
      $out .= "<td nowrap>" . $row['NewName'] . "</td>";
      $out .= "</tr>";
    }
    if (count($results["rows"]) <= 0)
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">No name change logs found.</td>";
      $out .= "</tr>";
    }
    else
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
      $out .= "</tr>";
    }
    $out .= "</table>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
