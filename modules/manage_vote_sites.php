<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Edit";
  $moduleLabel = "Manage Vote Sites";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $gen = isset($_GET['page_gen']) ? $_GET['page_gen'] : "1";
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";

    $links = array ();
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "'>View Sites</a>";
    $links[] = "<a href='./" . $script_name . "?action=" . $_GET['action'] . "&page=addedit'>Add Site</a>";

    if ($page == "")
    {
      $active_index = 0;
    }
    else if ($page == "addedit")
    {
      $active_index = 1;
    }
    else if ($page == "delete" || $page == "delete_site")
    {
      $active_index = 2;
      $links[] = "<a href='#'>Delete Item</a>";
    }

    $out .= generate_module_nav_html($links, $active_index);

    $attempt = get_vote_sites();


    if (empty($page))
    {
      $out .= "<table class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
      $out .= "<tr>";
      $out .= "<td class=\"thead\" style=\"text-align: center;\" nowrap>ID</td>";
      $out .= "<td class=\"thead\" nowrap>Site Name</td>";
      $out .= "<td class=\"thead\" nowrap>Site Image</td>";
      $out .= "<td class=\"thead\" nowrap>Reset Time</td>";
      $out .= "<td class=\"thead\" colspan=\"2\" nowrap>Options</td>";
      $out .= "</tr>";

      if ($attempt["error"] == True)
      {
        $out .= get_notification_html($attempt["errorMessage"], ERROR);
      }

      foreach ($attempt["rows"] as $key => $row)
      {
        $out .= "<tr>";
        $out .= "<td valign=\"top\" style=\"text-align: center;\" nowrap>" . $row['vote_id'] . "</td>";
        $out .= "<td valign=\"top\" nowrap>" . $row['vote_site_name'] . "</td>";
        $out .= "<td valign=\"top\" style=\"text-align: center;\" width=\"1\" nowrap><a href=\"" . $row['vote_site_url'] . "\"><img src=\"" . $row['vote_site_image'] . "\" width=\"50\" /></a></td>";
        $out .= "<td valign=\"top\" nowrap>" . $row['vote_reset_time'] . "s</td>";
        $out .= "<td style=\"text-align: center;\" nowrap><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&page=addedit&vote_id=" . $row['vote_id'] . "\"  style=\"text-decoration: none;\">Edit</a></td>";
        $out .= "<td style=\"text-align: center;\" nowrap><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&page=delete&vote_id=" . $row['vote_id'] . "\"  style=\"text-decoration: none;\">Delete</a></td>";
        $out .= "</tr>";
      }

      if (count($attempt["rows"]) <= 0)
      {
        $out .= "<tr>";
        $out .= "<td colspan=\"7\" style=\"text-align: center; font-weight: bold;\">No vote for NC sites have been added.</td>";
        $out .= "</tr>";
      }
      $out .= "</table>";
    }
    else
    {
      if ($page == "addedit")
      {
        $display_form = true;
        $do_process = 0;
        $exit_process = false;
        $exit_text = "";
        $add_submit = isset($_POST['add_submit']) ? 1 : 0;
        $edit_submit = isset($_POST['edit_submit']) ? 1 : 0;
        $vote_id = isset($_REQUEST['vote_id']) && ctype_digit($_REQUEST['vote_id']) ? $_REQUEST['vote_id'] : "";
        $vote_site_name = isset($_POST['vote_site_name']) ? antiject($_POST['vote_site_name']) : "";
        $vote_site_url = isset($_POST['vote_site_url']) ? antiject($_POST['vote_site_url']) : "";
        $vote_site_image = isset($_POST['vote_site_image']) ? antiject($_POST['vote_site_image']) : "";
        $vote_reset_time = isset($_POST['vote_reset_time']) ? antiject($_POST['vote_reset_time']) : "43200";

        $messages = array ();

        if ($add_submit == 1 || $edit_submit == 1)
        {
          $do_process = 1;
        }
        if ($vote_id != "")
        {
          $page_mode = "edit_submit";
          $submit_name = "Update Site";
          $this_mode_title = "Edit Site";
          $disable = " disabled";
          if ($do_process == 0)
          {
            if (!array_key_exists($vote_id, $attempt["rows"]))
            {
              $display_form = false;
              $out .= get_notification_html("Could not find vote site", ERROR);
            }
            else
            {
              $info = $attempt["rows"][$vote_id];
              $vote_site_name = $info['vote_site_name'];
              $vote_site_url = $info['vote_site_url'];
              $vote_site_image = $info['vote_site_image'];
              $vote_reset_time = $info['vote_reset_time'];
            }
          }
        }
        else
        {
          $page_mode = "add_submit";
          $submit_name = "Add site";
          $this_mode_title = "Adding a new vote site";
          $disable = "";
        }
        if ($do_process == 1)
        {
          if ($vote_site_name == "")
          {
            $messages[] = "You have not filled in a name for this site";
          }
          if ($vote_reset_time == "")
          {
            $messages[] = "You have not filled in a reset time (in seconds)";
          }
          if (!ctype_digit($vote_reset_time))
          {
            $messages[] = "Invalid reset time given";
          }
          if (!filter_var($vote_site_url, FILTER_VALIDATE_URL))
          {
            $messages[] = "You have entered an invalid website url (make sure you include http://)";
          }
          if (!filter_var($vote_site_image, FILTER_VALIDATE_URL))
          {
            $messages[] = "You have entered an invalid image url (make sure you include http://)";
          }
        }
        if (count($messages) > 0)
        {
          $out .= get_notification_html($messages, ERROR);
        }
        else if ($add_submit == 1)
        {
          $add_site = add_vote_site($vote_site_name, $vote_site_url, $vote_site_image, $vote_reset_time);
          if ($add_site["error"] == True)
          {
            $out .= get_notification_html($add_site["errorMessage"], ERROR);
          }
          else
          {
            $out .= get_notification_html("Successfully added the site " . $vote_site_name, SUCCESS);
            gamecp_log(0, $userdata -> username, "ADMIN - VOTE SITES - ADDED - Site Name: {$vote_site_name}", 1);
            $display_form = false;
            $redirect = CUSTOM_PAGE_SHORT;
            $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
          }
        }
        else if ($edit_submit == 1)
        {
          $update_site = update_vote_site($vote_id, $vote_site_name, $vote_site_url, $vote_site_image, $vote_reset_time);
          if ($update_site["error"] == True)
          {
            $out .= get_notification_html($update_site["errorMessage"], ERROR);
          }
          else
          {
            $out .= get_notification_html("Successfully updated the site " . $vote_site_name, SUCCESS);
            gamecp_log(0, $userdata -> username, "ADMIN - VOTE SITES - UPDATED - Site ID: {$vote_id}", 1);
            $display_form = false;
            $redirect = CUSTOM_PAGE_SHORT;
            $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
          }
        }
        if ($display_form == true)
        {
          $out .= "<form class=\"ink-form\" method=\"post\">";
          $out .= "<table class=\"ink-form\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
          $out .= "<tr>";
          $out .= "<td class=\"thead\" colspan=\"2\">" . $this_mode_title . "</td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td width=\"1\" nowrap>Site Name:</td>";
          $out .= "<td><input type=\"text\" name=\"vote_site_name\" value=\"" . $vote_site_name . "\"/></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td width=\"1\" nowrap>Reset Time:</td>";
          $out .= "<td><input type=\"text\" name=\"vote_reset_time\" value=\"" . $vote_reset_time . "\"/> (seconds)</td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td width=\"1\" nowrap>Site URL:</td>";
          $out .= "<td><input type=\"text\" name=\"vote_site_url\" value=\"" . $vote_site_url . "\" size=\"50\"/></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td width=\"1\" nowrap>Site Image URL:</td>";
          $out .= "<td><input type=\"text\" name=\"vote_site_image\" value=\"" . $vote_site_image . "\"  size=\"50\"/></td>";
          $out .= "</tr>";
          $out .= "<tr>";
          $out .= "<td colspan=\"2\" nowrap>";
          $out .= "<input name=\"vote_id\" type=\"hidden\" value=\"" . $vote_id . "\"/>";
          $out .= "<input name=\"page\" type=\"hidden\" value=\"addedit\"/>";
          $out .= "<input class=\"ink-button\" name=\"" . $page_mode . "\" type=\"submit\" value=\"" . $submit_name . "\"/></td>";
          $out .= "</tr>";
          $out .= "</table>";
          $out .= "</form>";
        }
      }
      else if ($page == "delete")
      {
        $vote_id = isset($_GET['vote_id']) && ctype_digit($_GET['vote_id']) ? $_GET['vote_id'] : "";
        if (!array_key_exists($vote_id, $attempt["rows"]))
        {
          $out .= get_notification_html("No site found", ERROR);
        }
        else
        {
          $out .= "<form class=\"ink-form\" method=\"post\">";
          $out .= "<p style=\"text-align: center; font-weight: bold;\">Are you sure you want to DELETE the site: <u>" . antiject($attempt["rows"][$vote_id]['vote_site_name']) . "</u> (ID: " . $vote_id . ")?</p>";
          $out .= "<p style=\"text-align: center;\"><input type=\"hidden\" name=\"vote_id\" value=\"" . $vote_id . "\"/><input type=\"hidden\" name=\"page\" value=\"delete_site\"/><input type=\"submit\" name=\"yes\" value=\"Yes\"/> <input type=\"submit\" name=\"no\" value=\"No\"/></p>";
          $out .= "</form>";
        }
      }
      else if ($page == "delete_site")
      {
        $yes = isset($_POST['yes']) ? "1" : "0";
        $no = isset($_POST['no']) ? "1" : "0";
        $vote_id = isset($_POST['vote_id']) && ctype_digit($_POST['vote_id']) ? $_POST['vote_id'] : "";

        if ($no != 1 && $vote_id != "")
        {
          if (!array_key_exists($vote_id, $attempt["rows"]))
          {
            $out .= get_notification_html("No site found", ERROR);
          }
          else
          {
            delete_vote_site($vote_id);
            $out .= get_notification_html("Successfully deleted the site " . $attempt["rows"][$vote_id]['vote_site_name'], SUCCESS);
            gamecp_log(2, $userdata -> username, "ADMIN - VOTE SITES - DELETED - Site Name:  " . antiject($attempt["rows"][$vote_id]['vote_site_name']) . " | ID: " . $vote_id, 1);
            $redirect = CUSTOM_PAGE_SHORT;
            $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
          }
        }
        else
        {
          $redirect = CUSTOM_PAGE_SHORT;
          $redirecturl = "./" . $script_name . "?action=" . $_GET['action'];
        }
      }
      else
      {
        $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
        $redirect = INDEX_PAGE_SHORT;
      }
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
