<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Logs";
  $moduleLabel = "Item Redeem Logs";
  $permission = "restricted";
  return;
}

if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    include( "./core/pagination.php" );

    $out .= "<div class=\"market_current_points_text\" style=\"width:100%; font-size: 14pt; text-align: center;\">You currently have <span style=\"color: #4259FF; font-weight:bold;\">" . number_format($userdata -> credits, 2) . "</span> Novus Credits( <img src=\"./framework/img/currency.png\"> )</div>";
    $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
    $out .= "<tr>";
    $out .= "<th style=\"text-align: center;\" nowrap>#</th>";
    $out .= "<th nowrap>Date</th>";
    $out .= "<th nowrap>Item Name</th>";
    $out .= "<th nowrap>Character Name</th>";
    $out .= "<th nowrap>Item Price</th>";
    $out .= "<th nowrap>GP After Purchase</th>";
    $out .= "</tr>";

    $sql = "SELECT R.redeem_char_id, R.redeem_price, R.redeem_item_id, R.redeem_total_gp, R.redeem_time, R.redeem_item_name,";
    $sql .= "I.item_name, I.item_delete ";
    $sql .= "FROM gamecp_redeem_log R LEFT JOIN gamecp_market_items I ON R.redeem_item_id = I.item_id ";
    $sql .= "WHERE R.redeem_id NOT IN ( SELECT TOP [OFFSET] R.redeem_id FROM gamecp_redeem_log R LEFT JOIN gamecp_market_items I ON R.redeem_item_id = I.item_id ";
    $sql .= "ORDER BY R.redeem_id DESC) ORDER BY R.redeem_id DESC";
    $sql_count = "Select COUNT(redeem_char_id) FROM gamecp_redeem_log";

    $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
    $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

    $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 30, $links_to_show = 10);
    $results = $pager -> get_data();

    $i = 0;
    if ($page_gen >= 1)
    {
      $i += ($page_gen - 1) * 25 + 1;
    }

    foreach ($results["rows"] as $key => $row)
    {
      $attempt = get_character_info($row['redeem_char_id']);
      $char = $attempt["char"];
      $char_name = ($char['Name'] != "") ? $char['Name'] : "Unknown";

      if ($char['DCK'] == 1)
      {
        $char_name = "<i>" . $char_name . "</i>";
      }

      if ($row['item_delete'] == 1)
      {
        $item_name = "<i>" . $row['item_name'] . "</i>";
      }
      else if ($row['redeem_item_name'] != "")
      {
        $item_name = $row['redeem_item_name'];
      }
      else
      {
        $item_name = "<i>Unknown</i>";
      }

      $out .= "<tr>";
      $out .= "<td style=\"text-align: center;\" nowrap>" . $i . "</td>";
      $out .= "<td nowrap>" . date("d/m/y h:i:s A", $row['redeem_time']) . "</td>";
      $out .= "<td nowrap>" . $item_name . "</td>";
      $out .= "<td nowrap>" . $char_name . "</td>";
      $out .= "<td nowrap>" . number_format($row['redeem_price'], 2) . " GP</td>";
      $out .= "<td nowrap>" . number_format($row['redeem_total_gp'], 2) . " GP</td>";
      $out .= "</tr>";
      ++$i;
    }
    if (count($results["rows"]) <= 0)
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">No redeem logs found for your account.</td>";
      $out .= "</tr>";
    }
    else
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
      $out .= "</tr>";
    }
    $out .= "</table>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
