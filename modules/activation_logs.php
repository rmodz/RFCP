<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Logs";
  $moduleLabel = "Activation Logs";
  $permission = "restricted";
  return;
}

if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    include( "./core/pagination.php" );

    $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
    $out .= "<tr>";
    $out .= "<th class='align-left'>ID</th>";
    $out .= "<th class='align-left'>Confirm Key</th>";
    $out .= "<th class='align-left'>Username</th>";
    $out .= "<th class='align-left'>Email</th>";
    $out .= "<th class='align-left'>Date</th>";
    $out .= "</tr>";

    $sql = "SELECT id, confirm_key, username, email, created_at ";
    $sql .= "FROM gamecp_confirm_email ";
    $sql .= "WHERE id NOT IN ( SELECT TOP [OFFSET] id FROM gamecp_confirm_email ";
    $sql .= "ORDER BY id DESC) ORDER BY id DESC";
    $sql_count = "Select COUNT(id) FROM gamecp_confirm_email";

    $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 0;
    $url = str_replace("&page_gen=" . $page_gen, "", $_SERVER['REQUEST_URI']);

    $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 30, $links_to_show = 10);
    $results = $pager -> get_data();

    foreach ($results["rows"] as $key => $row)
    {
      $out .= "<tr>";
      $out .= "<td nowrap>" . $row['id'] . "</td>";
      $out .= "<td nowrap>" . $row['confirm_key'] . "</td>";
      $out .= "<td nowrap>" . $row['username'] . "</td>";
      $out .= "<td nowrap>" . $row['email'] . "</td>";
      $out .= "<td nowrap>" . date("d/m/y h:i:s A", $row['created_at']) . "</td>";
      $out .= "</tr>";
    }
    if (count($results["rows"]) <= 0)
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">No activation logs found.</td>";
      $out .= "</tr>";
    }
    else
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"6\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
      $out .= "</tr>";
    }
    $out .= "</table>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
