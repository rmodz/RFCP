<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Logs";
  $moduleLabel = "Vote Logs";
  $permission = "restricted";
  return;
}

if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $sort_by = isset($_GET["sortby"]) ? $_GET["sortby"] : "";
    $ip = isset($_POST['ip']) ? $_POST['ip'] : "";
    $page_gen = isset($_REQUEST['page_gen']) ? intval($_REQUEST['page_gen']) : 1;
    if (isset($_GET['ip']) && $_GET['ip'] != "")
    {
      $ip = $_GET['ip'];
    }
    if (isset($_GET['ban']) && $_GET['ban'] = !"" && $_GET['ban'] == "ban")
    {
      $gm_name = $userdata -> username;

      ban_user($_GET['serial'], 119988, "Multiple Account Voting", 0, $gm_name);
      gamecp_log(4, $userdata -> username, "ADMIN - VOTE LOGS BAN - ADDED - Account Serial: {$_GET['serial']}", 1);
    }
    if (isset($_POST['submit']) && $_POST['submit'] == 'delete')
    {
      $con = connectdb(RFCP);
      $sql = "DELETE FROM gamecp_vote_log";
      $result = sqlsrv_query($con, $sql);

      gamecp_log(4, $userdata -> username, "ADMIN - VOTE LOGS - DELETE - Log table cleared", 1);
    }

    $url = "" . $script_name . "?action=" . $_GET['action'] . "&sortby=" . $sort_by . "&ip=" . $ip;

    include( "./core/pagination.php" );

    $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
    $out .= "<th class='align-left' style = 'border-color:#FFFFFF;'><form id ='input' method = 'post' action ='$url'><input type='text' name='ip' style = 'width:200px; padding-left: 5px' placeholder='Press enter to search IP' value = '" . $ip . "'/></form></th>";
    $out .= "<tr>";
    $out .= "<th class='align-left'><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&sortby=id\">Serial</a></th>";
    $out .= "<th class='align-left'><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&sortby=account\">Account ID</a></th>";
    $out .= "<th class='align-left'>Time</th>";
    $out .= "<th class='align-left'><a href=\"" . $script_name . "?action=" . $_GET['action'] . "&sortby=ip\">IP</a></th>";
    $out .= "<th class='align-left'>NC Gained</th>";
    $out .= "<th class='align-left'>NC Total</th>";
    $out .= "<th class='align-left'>Site ID</th>";
    $out .= "<th class='align-left'>Ban</th>";
    $out .= "</tr>";


    if ($sort_by == "ip")
    {
      $sort = "log_ip";
    }
    else if ($sort_by == "account")
    {
      $sort = "log_account_serial";
    }
    else if ($sort_by == "id")
    {
      $sort = "log_id";
    }
    else
    {
      $sort = "log_id";
    }
    $ipsql1 = "";
    $ipsql2 = "";
    if (isset($ip) && $ip != "")
    {
      $ipsql1 = "log_ip = '" . $ip . "' AND ";
      $ipsql2 = "WHERE log_ip = '" . $ip . "'";
    }

    $sql = "SELECT log_id, log_account_serial, log_time, log_ip, log_points_gained, log_total_points, site_id ";
    $sql .= "FROM gamecp_vote_log ";
    $sql .= "WHERE " . $ipsql1 . " log_id NOT IN ( SELECT TOP [OFFSET] log_id FROM gamecp_vote_log " . $ipsql2 . " ";
    $sql .= "ORDER BY " . $sort . " DESC) ORDER BY " . $sort . " DESC";
    $sql_count = "Select COUNT(log_id) FROM gamecp_vote_log " . $ipsql2 . "";
    $pager = new Pagination(RFCP, $sql, $sql_count, $url, array (), array (), $page_size = 30, $links_to_show = 10);
    $results = $pager -> get_data();

    foreach ($results["rows"] as $key => $row)
    {
      $attempt = get_ban_record($row['log_account_serial']);

      if ($attempt["error"] != true)
      {
        $bgcolor = "color: red;";
      }
      else
      {
        $bgcolor = "";
      }

      $out .= "<tr>";
      $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row['log_id'] . "</td>";
      $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row['log_account_serial'] . "</td>";
      $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . date("d/m/y h:i:s A", $row['log_time']) . "</td>";
      $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row['log_ip'] . "</td>";
      $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row['log_points_gained'] . " GP</td>";
      $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row['log_total_points'] . " GP</td>";
      $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row['site_id'] . "</td>";

      $out .= "<td nowrap><a class='icon_link " . BASE_COLOR . " 'href='" . $url . "&serial=" . $row['log_account_serial'] . "&ban=ban&page_gen=" . $page_gen . "' ><i class='icon-ban-circle icon-large' title='Ban Account'></i></a></td>";
      $out .= "</tr>";
    }
    if (count($results["rows"]) <= 0)
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"8\" style=\"text-align: center; font-weight: bold;\">No Vote logs found.</td>";
      $out .= "</tr>";
    }
    else
    {
      $out .= "<tr>";
      $out .= "<td colspan=\"8\" style=\"text-align: center; font-weight: bold;\">" . $pager -> renderFullNav() . "</td>";
      $out .= "</tr>";
    }
    $out .= "</table>";
    $out .= "<form id ='submit' method = 'post' action ='" . $script_name . "?action=" . $_GET['action'] . "'><input type='submit' name='submit' class='ink-button' value = 'delete'/></form>";
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}

