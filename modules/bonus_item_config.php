<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Administration";
  $moduleLabel = "Bonus Item Config";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    if (isset($_POST['page']) || isset($_GET['page']))
    {
      $page = isset($_POST['page']) ? $_POST['page'] : $_GET['page'];
    }
    else
    {
      $page = "";
    }

    $character_serial = (isset($_POST['character_serial']) || isset($_GET['character_serial'])) ? (isset($_POST['character_serial']) ? $_POST['character_serial'] : $_GET['character_serial']) : "";
    $character_serial = ctype_digit($character_serial) ? antiject($character_serial) : "";
    $character_name = (isset($_POST['character_name']) || isset($_GET['character_name'])) ? (isset($_POST['character_name']) ? $_POST['character_name'] : $_GET['character_name']) : "";
    $character_name = trim(antiject($character_name));

    if ($page == "")
    {
      $attempt = get_bonus_items_from_config();
      if ($attempt["error"] == True)
      {
        $out .= get_notification_html($attempt["errorMessage"], ERROR);
      }
      else
      {
        $out .= "<center><h5>Current Bonus Items</h5></center>";
        $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
        $out .= "<tr>";
        $out .= "<td nowrap>&nbsp;</td>";
        $out .= "<td nowrap>Item Name</td>";
        $out .= "<td nowrap>Item Race</td>";
        $out .= "<td nowrap>Amount</td>";
        $out .= "<td nowrap>Rental Time</td>";
        $out .= "<td nowrap>Upgrade</td>";
        $out .= "<td nowrap>Delete</td>";
        $out .= "</tr>";
        if (0 < count($attempt["rows"]))
        {
          foreach ($attempt["rows"] as $key => $row)
          {
            $k_value = $row['bonus_item_k'];
            $u_value = $row['bonus_item_u'];
            $item_details = get_item_details($k_value, $u_value);
            $upgrades = $item_details["images"];
            $attempt = get_item_info_from_id($item_details["type"], $item_details["id"]);
            $item_code = $attempt["item"]["item_code"];
            $item_name = str_replace("_", " ", $attempt["item"]["item_name"]);
            $image = get_item_image($attempt["item"]["item_icon_id"], $item_details["type"]);

            if ($row['bonus_race'] == 1)
            {
              $race = "<span style=\"color: #CC6699;\">Belleto</span>";
            }
            else if ($row['bonus_race'] == 2)
            {
              $race = "<span style=\"color: #9933CC;\">Cora</span>";
            }
            else if ($row['bonus_race'] == 3)
            {
              $race = "<span style=\"color: grey;\">Accreatian</span>";
            }
            else if ($row['bonus_race'] == 4)
            {
              $race = "<span style=\"color: #CC6699;\">Belleto</span> & <span style=\"color: #9933CC;\">Cora</span>";
            }
            else
            {
              $race = "All Races";
            }

            $out .= "<tr>";
            $out .= "<td nowrap><img class=\"market_item_image\" src=\"" . $image . "\" style='height: 40px; width: 40px;' /></td>";
            $out .= "<td nowrap>" . $item_name . " (" . $item_code . ")</td>";
            $out .= "<td nowrap>" . $race . "</td>";
            $out .= "<td nowrap>" . $row['bonus_item_d'] . "</td>";
            $out .= "<td nowrap>" . $upgrades . "</td>";
            $out .= "<td nowrap>" . $row['bonus_item_t'] . "</td>";
            $out .= "<td><a class='icon_link " . BASE_COLOR . " 'href='" . $script_name . "?action=" . $_GET['action'] . "&page=delete&delete_id=" . $row["id"] . "'><i class='icon-trash icon-large' title='Delete Item'></i></a></td>";
            $out .= "</tr>";
          }
        }
        else
        {
          $out .= "<tr>";
          $out .= "<td colspan=\"7\" style=\"text-align: center;\">No bonus items set</td>";
          $out .= "</tr>";
        }
        $out .= "</table>";

        $out .= "<br/>";
        $out .= "<center><h5>Add Item(s)</h5></center>";
        $out .= "<form class=\"ink-form\" action=\"?action=" . $_GET['action'] . "\" method=\"post\" id=\"form1\">";
        $out .= "<table id=\"myTable\" class=\"tborder\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
        $out .= "<tr>";
        $out .= "<td nowrap>#</td>";
        $out .= "<td nowrap>Item Name</td>";
        $out .= "<td nowrap>Item Code</td>";
        $out .= "<td nowrap>Item Race</td>";
        $out .= "<td nowrap>Amount</td>";
        $out .= "<td nowrap>Upgrade</td>";
        $out .= "<td nowrap>Rental Period</td>";
        $out .= "</tr>";
        $out .= "<input type=\"hidden\" id=\"max_row_id\" value=\"row_0\" />";
        $out .= "<tr id=\"row_0\" class=\"item_give_row\">";
        $out .= "<td nowrap>0</td>";
        $out .= "<td nowrap style=\"width: 400px;\"><div id=\"results_row_0\">-1</div></td>";
        $out .= "<td nowrap><input type=\"text\" width=\"100%\" name=\"item_code[]\" onkeyup=\"check_itemname(this, 'results_row_0');\"/></td>";

        $out .= "<td nowrap>";
        $out .= "<select name=\"item_race[]\">";
        $m = 0;
        while ($m <= 4)
        {
          if ($m == 0)
          {
            $race = "All Races";
          }
          else if ($m == 1)
          {
            $race = "Bellato";
          }
          else if ($m == 2)
          {
            $race = "Cora";
          }
          else if ($m == 3)
          {
            $race = "Accretia";
          }
          else if ($m == 4)
          {
            $race = "Bellato & Cora";
          }

          $out .= "<option value=\"" . $m . "\">" . $race . "</option>";
          ++$m;
        }
        $out .= "</select>";
        $out .= "</td>";

        $out .= "<td nowrap><input type=\"text\" width=\"100%\" name=\"item_amount[]\" value=\"0\"/></td>";
        $out .= "<td nowrap style=\"width: 200px;\">" . generate_upgrade_multi_input_html(array ()) . "</td>";
        $out .= "<td nowrap><input type=\"text\" width=\"100%\" name=\"item_rental_time[]\" value=\"0\"/></td>";
        $out .= "</tr>";
        $out .= "<tr id=\"last\">";
        $out .= "<td colspan=\"6\"><input type=\"hidden\" name=\"page\" value=\"add_item\" /><input class=\"ink-button\" type=\"submit\" value=\"Submit\" name=\"submit\"><input class=\"ink-button\" type=\"reset\" value=\"Reset\" name=\"reset\"></td>";
        $out .= "<td style=\"text-align: right;\" nowrap><a href=\"#\" onClick=\"addGiveItemField(); return false;\">Add More Items</a></td>";
        $out .= "</tr>";
        $out .= "</table>";
        $out .= "</form>";
        $out .= "<br/>";
      }
    }
    else if ($page == "add_item")
    {
      $page_exit = false;

      $item_code_x = isset($_POST['item_code']) ? $_POST['item_code'] : "";
      $item_amount_x = isset($_POST['item_amount']) ? $_POST['item_amount'] : "";
      $item_rental_time_x = isset($_POST['item_rental_time']) ? $_POST['item_rental_time'] : "";
      $item_slots_x = isset($_POST['item_slots']) ? $_POST['item_slots'] : 0;
      $item_upgrades_x = isset($_POST['talics']) ? $_POST['talics'] : array ();
      $item_races_x = isset($_POST['item_race']) ? $_POST['item_race'] : 0;

      $itemcode_count = count($item_code_x);
      $itemamount_count = count($item_amount_x);
      $itemslots_count = count($item_slots_x);
      $itemrentaltime_count = count($item_rental_time_x);
      if ($itemcode_count != $itemamount_count || $itemcode_count != $itemslots_count || $itemcode_count != $itemrentaltime_count)
      {
        $page_exit = true;
        $out .= get_notification_html("Error! Seems you are missing a field somewhere", ERROR);
      }

      if (!$page_exit)
      {
        $i = 0;
        while ($i < $itemcode_count)
        {
          $messages = array ();
          $add_item = true;
          $item_code = !empty($item_code_x[$i]) ? antiject($item_code_x[$i]) : "";
          $item_amount = !empty($item_amount_x[$i]) && ctype_digit($item_amount_x[$i]) ? intval($item_amount_x[$i]) : 0;
          $item_slots = !empty($item_slots_x[$i]) && ctype_digit($item_slots_x[$i]) ? intval($item_slots_x[$i]) : 0;
          $item_upgrades = !empty($item_upgrades_x[$i]) ? $item_upgrades_x[$i] : array ();
          $item_rental_time = !empty($item_rental_time_x[$i]) && ctype_digit($item_rental_time_x[$i]) ? intval($item_rental_time_x[$i]) : 0;
          $item_race = !empty($item_races_x[$i]) && ctype_digit($item_races_x[$i]) ? intval($item_races_x[$i]) : 0;

          if ($item_code == "" && $item_amount == "" && $item_ups == "" && $item_slots == "" && $item_talic == "" && $item_rental_time == "")
          {
            $messages[] = "Row " . ($i + 1) . ": Sorry, you have left a field empty";
          }

          if (count($messages) == 0)
          {
            $item_kind = parse_item_code($item_code);
            $item_ups = 0;
            if (count($item_upgrades) > 0)
            {
              $empty = array ();
              $item_ups = 0;
              foreach ($item_upgrades as $index => $talic)
              {
                $temp = intval($talic);
                if ($temp < 0 || $temp == 14 || $temp > 15)
                {
                  $messages[] = "Row " . ($i + 1) . ": Talic " . ($index + 1) . " is not a valid talic.";
                }

                if ($temp > 0 && $temp < 14 && count($empty) > 0)
                {
                  $messages[] = "Row " . ($i + 1) . ": Talic " . ($index + 1) . " cannot be set if all previous talics are not set.";
                }
                else if ($temp > 0 && $temp < 14)
                {
                  $item_ups++;
                }

                if ($temp == 15)
                {
                  $empty[] = $index;
                }
              }
            }

            $attempt = get_item_info_from_code($item_code);
            if ($attempt["error"] === True)
            {
              $messages[] = "Row " . ($i + 1) . ": Error for item " . $item . ": " . $attempt["errorMessage"];
            }
            else
            {
              $item_info = $attempt["item"];
            }

            $item_id = $item_info["item_id"];
            $item_kind = parse_item_code($item_code);
            $table_name = get_table_name($item_kind);

            if (count($messages) == 0)
            {
              if (in_array($item_kind, $upgrade_types))
              {
                $item_amount = 0;
              }
              else if ($item_amount < 1)
              {
                $item_amount = 1;
              }

              //go ahead and parse the talics
              if ($item_slots < $item_ups)
              {
                $item_ups = $item_slots;
              }
              if ($item_slots > 7)
              {
                $item_slots = 7;
              }
              if (count($item_upgrades) < 7)
              {
                $iUpgrade = count($item_upgrades);
                while ($iUpgrade < 7)
                {
                  $item_upgrades[] = 15;
                  $iUpgrade++;
                }
              }

              $item_upgrade = $item_slots . "";
              foreach (array_reverse($item_upgrades) as $index => $talic)
              {
                $item_upgrade .= dechex($talic);
              }
              $item_upgrade = hexdec($item_upgrade);
              //end talic parsing

              if (!in_array($item_kind, $upgrade_types))
              {
                $item_upgrade = BASE_CODE;
              }

              $item_db_code = get_item_db_code($item_id, $item_kind, 0);

              $attempt = add_bonus_item_to_config($item_db_code, $item_upgrade, $item_amount, $item_rental_time, $item_race);
              if ($attempt["error"] == True)
              {
                $messages[] = "Row " . ($i + 1) . ": Error while inserting item, " . $item_code . " . This item was not added.";
              }
              else
              {
                gamecp_log(2, $userdata -> username, "ADMIN - BONUS ITEM ADDED TO CONFIG - Item Code: {$item_code} [" . dechex($item_upgrade) . "]", 1);
              }
            }
          }
          ++$i;
        }
        if (count($messages) > 0)
        {
          $out .= get_notification_html($messages, ERROR);
        }
        else
        {
          $out .= get_notification_html("Item(s) successfully added", SUCCESS);
          $redirect = PREVIOUS_PAGE_SHORT;
        }
      }
    }
    else if ($page == "delete")
    {
      $bonus_id = isset($_GET['delete_id']) ? intval($_GET['delete_id']) : -1;
      delete_bonus_item($bonus_id);
      $out .= get_notification_html("Bonus item removed", SUCCESS);
      $redirect = PREVIOUS_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
