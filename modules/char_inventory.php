<?php

if (!empty($loadingmodules))
{
  $file = basename(__FILE__);
  $moduleCategory = "Edit";
  $moduleLabel = "Char Inventory";
  $permission = "restricted";
  return;
}
if ($this_script == $script_name)
{
  if ($userdata -> has_permission($action))
  {
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "";
    $search = isset($_GET['search']) ? $_GET['search'] : "";
    $delete = isset($_GET['delete']) ? $_GET['delete'] : "";
    $character_name = isset($_GET['character_name']) ? antiject($_GET['character_name']) : "";
    $character_serial = isset($_GET['character_serial']) && ctype_digit($_GET['character_serial']) ? intval($_GET['character_serial']) : 0;
    $search_query = "";
    $num_of_bags = 100;
    if (empty($page))
    {
      $out .= "<form class=\"ink-form\" method=\"GET\" action=\"" . $script_name . "?action=" . $_GET['action'] . "\">";
      $out .= "<table class=\"tborder\" cellpadding=\"2\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
      $out .= "<tr>";
      $out .= "<td>Charcter Serial:</td>";
      $out .= "<td><input type=\"text\" name=\"character_serial\" /></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td>Charcter Name:</td>";
      $out .= "<td><input type=\"text\" name=\"character_name\" /></td>";
      $out .= "</tr>";
      $out .= "<tr>";
      $out .= "<td colspan=\"2\"><input type=\"hidden\" name=\"action\" value=\"" . $_GET['action'] . "\" /><input class=\"ink-button\" type=\"submit\" value=\"Search\" name=\"search\" /></td>";
      $out .= "</tr>";
      $out .= "</table>";
      $out .= "</form>";

      $messages = array ();

      if ($delete != "")
      {
        $slot = isset($_GET['slot']) ? intval($_GET['slot']) : "";
        $serial = isset($_GET['serial']) ? intval($_GET['serial']) : "";
        $attempt = get_character_info($serial);
        $dbcon = connectdb(DATA);

        $sql = "SELECT k{$slot}, u{$slot}, d{$slot} FROM tbl_inven WHERE serial = ?";
        $results = sqlsrv_query($dbcon, $sql, array ($serial));
        $row = sqlsrv_fetch_array($results);
        $k_value = $row["k{$slot}"];
        $u_value = $row["u{$slot}"];
        $d_value = $row["d{$slot}"];

        $sql = "UPDATE tbl_inven SET k{$slot} = '-1',u{$slot} = '-1',d{$slot} = '0', s{$slot} = '0', t{$slot} = '-1' WHERE Serial = ?";
        $results = sqlsrv_query($dbcon, $sql, array ($serial));

        item_delete_log($attempt["char"]["AccountSerial"], $attempt["char"]["Serial"], $k_value, $u_value, $d_value, -1, $userdata -> serial);
        gamecp_log(0, $userdata -> username, "ADMIN - CHAR INVENTORY - Deleted slot {$slot} for character {$character_name} - {$serial}", 3);
      }

      if ($search != "")
      {
        $out .= "<br/><br/>";
        if ($character_name == "" && $character_serial == 0)
        {
          $messages[] = "Sorry, make sure you filled in either the character serial or name";
        }

        if (count($messages) == 0)
        {
          $bag_numbers = "";
          $i = 0;
          while ($i < 100)
          {
            $bag_numbers .= ", k{$i}, u{$i}, d{$i}";
            ++$i;
          }

          if ($character_serial < 1)
          {
            $character_serial = $character_name;
          }

          $attempt = get_character_info($character_serial);
          if ($attempt["error"] == False)
          {
            $character_serial = $attempt["char"]["Serial"];
            $out .= "<h5>Inventory for " . filter_string_for_html($attempt["char"]["Name"]) . "</h5>";
            if ($attempt["char"]["AccountSerial"] < 2000000000)
            {
              $user_data = get_user_data($attempt["char"]["AccountSerial"]);
              $t_login = $user_data["data"]['lastlogintime'];
              $t_logout = $user_data["data"]['lastlogofftime'];
              if ($t_login > $t_logout)
              {
                $out .= "<span style=\"font-weight: bold; color: red\">The user may still be logged in!</span>";
              }
              else
              {
                $out .= "<span style=\"font-weight: bold; color: green\">The user is logged out!</span>";
              }
            }
          }
          else
          {
            $character_serial = -1;
          }

          $sql = "SELECT Serial {$bag_numbers} FROM tbl_inven WHERE Serial = ?";
          $sql .= " ORDER BY Serial DESC";

          $dbcon = connectdb(DATA);

          $results = sqlsrv_query($dbcon, $sql, array ($character_serial));
          $out .= "<table class=\"ink-table\" cellpadding=\"3\" cellspacing=\"1\" border=\"0\" width=\"100%\">";
          $out .= "<tr>";
          $out .= "<th class=\"thead align-left\" nowrap>Slot</th>";
          $out .= "<th class=\"thead align-left\" nowrap>Item Code</th>";
          $out .= "<th class=\"thead align-left\" nowrap>Item Name</th>";
          $out .= "<th class=\"thead align-left\" nowrap>Amount</th>";
          $out .= "<th class=\"thead align-left\" nowrap>Upgrades</th>";
          $out .= "<th class=\"thead align-left\" nowrap>Delete</th>";
          $out .= "</tr>";

          $has_results = false;

          while ($row = sqlsrv_fetch_array($results, SQLSRV_FETCH_ASSOC))
          {
            $has_results = true;
            $i = 0;

            for ($i = 0; $i < $num_of_bags; $i++)
            {
              $k_value = $row["k{$i}"];
              $u_value = $row["u{$i}"];

              $item_details = array ();

              if (-1 == $k_value)
              {
                $k_value = "";
                $item_code = "-";
                $images = "";
                $item_image = "";
              }
              else
              {
                $item_details = get_item_details($k_value, $u_value);
                $item_info = get_item_info_from_id($item_details["type"], $item_details["id"]);
                $item_code = $item_info["item"]["item_code"];
                $item_id = $item_info["item"]["item_name"];
                $images = $item_details["images"];
                $icon_id = $item_info["item"]["item_icon_id"];
                $item_path = glob("./images/items/" . get_image_folder_name($item_details["type"]) . "/(" . $item_info["item"]["item_icon_id"] . "){.jpg,.JPG,.gif,.GIF,.png,.PNG}", GLOB_BRACE);
                if (file_exists(@$item_path[0]))
                {
                  $item_image = "<img id=\"\" class=\"market_item_image\" src=\"" . $item_path[0] . "\" style=\"height: 40px; width: 40px;\">";
                }
                else
                {
                  $item_image = "";
                }
              }

              if ($search != "" && $item_code != "-")
              {
                $out .= "<tr>";
                $out .= "<td nowrap>" . $i . "</td>";
                $out .= "<td nowrap>" . $item_code . "</td>";
                $out .= "<td nowrap>" . $item_image . " " . $item_id . "</td>";
                $out .= "<td nowrap>" . $row["d{$i}"] . "</td>";
                $out .= "<td nowrap>" . $images . "</td>";
                $out .= "<td><a class='icon_link " . BASE_COLOR . " 'href='" . $script_name . "?action=" . $_GET['action'] . "&character_name=" . $character_name . "&character_serial=" . $character_serial . "&search=" . $search . "&delete=delete&slot=" . $i . "&serial=" . $row['Serial'] . "'><i class='icon-trash icon-large' title='Delete Item'></i></a></td>";
                $out .= "</tr>";
              }
              else if ($item_code == "-")
              {
                $bgcolor = "color: blue;";
                $out .= "<tr>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $i . "</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $item_code . "</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>Empty Slot</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $row["d{$i}"] . "</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>" . $images . "</td>";
                $out .= "<td style=\"" . $bgcolor . "\" nowrap>-</td>";
                $out .= "</tr>";
              }
            }
          }

          if (!$has_results)
          {
            $out .= "<tr>";
            $out .= "<td colspan=\"5\" style=\"text-align: center;\">No such user/item found in the database</td>";
            $out .= "</tr>";
          }

          $out .= "</table>";

          gamecp_log(0, $userdata -> username, "ADMIN - INV EDIT - Searched for: {$character_name} or {$character_serial}", 1);
        }

        if (count($messages) > 0)
        {
          $out .= get_notification_html($messages, ERROR);
        }
      }
    }
    else
    {
      $out .= get_notification_html(PAGE_NOT_FOUND, ERROR);
      $redirect = INDEX_PAGE_SHORT;
    }
  }
  else
  {
    $out .= get_notification_html(INVALID_PERMISSION, ERROR);
    $redirect = INDEX_PAGE_SHORT;
  }
}
else
{
  $out .= get_notification_html(INVALID_LOAD, ERROR);
  $redirect = INDEX_PAGE_SHORT;
}
