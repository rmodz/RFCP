<?php

define("IN_GAMECP_SALT58585", true);
include( "./core/common.php" );
$lefttitle = "Login";
$title = $program_name . " - " . $lefttitle;
$exit_stage1 = false;
$exit_stage2 = false;
$notuser = true;
$isuser = false;
$gamecp_nav = "";

$username = isset($_POST['username']) ? trim($_POST['username']) : "";
$password = isset($_POST['password']) ? trim($_POST['password']) : "";

unset($_COOKIE['rfcp_username']);
unset($_COOKIE['rfcp_session']);

$error_message = "";
if ($username != "" && $password != "")
{
  $ip = $_SERVER['REMOTE_ADDR'];
  $privatekey = get_config_item("security_recaptcha_private_key");
  if (!preg_match(REGEX_USERNAME, $username) || !preg_match(REGEX_PASSWORD, $password))
  {
    $error_message = "Invalid Username or Password. Please try again.";
    $exit_stage1 = true;
  }
  if (isset($config['security_recaptcha_enable']) && $config['security_recaptcha_enable'] == 1 && $privatekey != "")
  {
    $resp = recaptcha_check_answer($privatekey, $_SERVER['REMOTE_ADDR'], $_POST['recaptcha_challenge_field'], $_POST['recaptcha_response_field']);
    if (!$resp -> is_valid && !strpos("/Input error: privatekey:/", $resp -> error))
    {
      $error_message = $resp -> error;
      $exit_stage1 = true;
    }
    if (strpos("/Input error: privatekey:/", $resp -> error))
    {
      gamecp_log(5, $userdata['username'], "RECAPTCHA - Invalid/Bad Private Key Supplied", 1);
    }
  }
}
else
{
  $error_message = "No Username or Password was provided";
  $exit_stage1 = true;
}
if ($exit_stage1 != true)
{
  $login_attempt = attempt_user_login($username, $password);

  if ($login_attempt["error"] === False)
  {
    setcookie('rfcp_username', $login_attempt["username"], time() + (60 * 60 * 24));
    setcookie('rfcp_session', $login_attempt["sessionid"], time() + (60 * 60 * 24));

    if (in_array($login_attempt['username'], $super_admin))
    {
      gamecp_log(3, $login_attempt['username'], "SUPER ADMIN - LOGGED IN");
    }
    else
    {
      gamecp_log(0, $login_attempt['username'], "USER - LOGGED IN");
    }
    header("Location: ./" . $script_name);
  }
  else
  {
    $error_message = $login_attempt["errorMessage"];
  }
}

if ($error_message != "")
{
  $out .= "<div class='ink-alert basic error'><center>" . $error_message . "</center></div>";
}

$out .= get_login_html();
$output = gamecp_template("gamecp");
print_outputs($output);
