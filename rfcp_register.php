<?php

define("IN_GAMECP_SALT58585", true);
include_once( "./includes/class.phpmailer.php" );
include( "./core/common.php" );
if ($isuser === true)
{
  header("Location: " . $script_name);
  exit();
}

$action = isset($_GET['action']) ? antiject($_GET['action']) : "home";
$username = isset($_POST['username']) ? antiject($_POST['username']) : "";
$password = isset($_POST['password']) ? antiject($_POST['password']) : "";
$confirm_password = isset($_POST['confirm_password']) ? antiject($_POST['confirm_password']) : "";
$email = isset($_POST['email']) ? antiject($_POST['email']) : "";
$confirm_email = isset($_POST['confirm_email']) ? antiject($_POST['confirm_email']) : "";
$user_ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "";
$register = isset($_POST['register']) ? true : false;
$referral = isset($_GET['ref']) && ctype_digit($_GET['ref']) ? intval($_GET['ref']) : 0;
$ref_id = 0;

if ($referral > 0)
{
  $ref_id = get_serial_from_ref($referral);
  $attempt = check_account_serial($ref_id);
  if ($attempt["error"] == True)
  {
    $ref_id = 0;
  }
}

if (!$config['security_max_accounts'])
{
  $config['security_max_accounts'] = 3;
}

$confirm_email_required = isset($config['security_confirm_email']) && $config['security_confirm_email'] == 1 ? True : False;

if ($action == "home")
{
  $out .= "<div class=\"column group\">";
  $out .= "<div class=\"large-100 push-center\">";
  $out .= "<center><strong>" . translation("Welcome to the user registration page.") . "</strong></center>";
  if ($confirm_email_required)
  {
    $out .= "<center><strong>" . translation("Please fill out the information below. Once you verify your e-mail address you will be asked to select a password for your account") . "</strong></center>";
  }
  $out .= "<center><strong>" . translation("Username and passwords are case-sensitive") . "</strong></center>";
  $out .= "</div><br/>";

  if ($register === true)
  {
    $securityLogin = isset($config["security_require_game_login"]) && $config["security_require_game_login"] == 1;
    $success = true;
    $message = array ();
    if ($username == "" || $email == "")
    {
      $success = false;
      $message[] = translation("All fields must be filled");
    }
    if (!preg_match(REGEX_USERNAME, $username))
    {
      $success = false;
      $message[] = translation("Invalid username provided. Username can only contain letters and numbers.");
    }
    if (strlen($username) < 4 || 12 < strlen($username))
    {
      $success = false;
      $message[] = translation("Username must be between 4 to 12 characters in length.");
    }
    if ($email != $confirm_email)
    {
      $success = false;
      $message[] = translation("E-Mail confirmation did not match. Please check for typos.");
    }
    else if (!is_email($email))
    {
      $success = false;
      $message[] = translation("Invalid e-mail address provided. Please make sure you enter a valid and working, email address");
    }
    if (!$confirm_email_required)
    {
      if ($password != $confirm_password)
      {
        $success = false;
        $message[] = translation("Confirmation password does not match. Please try again.");
      }
      else if (!preg_match(REGEX_PASSWORD, $password))
      {
        $success = false;
        $message[] = translation("Invalid password provided. Password can only contain letters and numbers.");
      }
      if (strlen($password) < 4 || 12 < strlen($password))
      {
        $success = false;
        $message[] = translation("Password must be between 4 to 24 characters in length.");
      }
    }
    if ($success === true)
    {
      if (!is_username_unique($username))
      {
        $success = False;
        $message[] = translation("Sorry, the username you choose has already been taken. Please choose another.");
      }
      if (!is_email_unique($email))
      {
        $success = False;
        $message[] = translation("Sorry, that email is already in use. Please choose another.");
      }
    }
    if ($success === true)
    {
      if (!$confirm_email_required)
      {
        $register_attempt = add_new_user($username, $password, $email, $user_ip, $securityLogin);
        if ($register_attempt["error"] == False)
        {
          $attempt = add_user_referral($ref_id, $username);
          header("Location: " . $_SERVER['PHP_SELF'] . "?action=success");
          exit();
        }
      }
      else
      {
        $confirm_key = getGUID();
        $confirm_key = substr($confirm_key, 1, strlen($confirm_key) - 2);
        $confirm_attempt = add_user_confirm($username, $email, $confirm_key);
        if ($confirm_attempt["error"] === True)
        {
          $success = false;
          $message[] = translation($confirm_attempt["errorMessage"]);
        }
        else
        {
          $confirm_url = get_url() . "?action=verify&confirm_key=" . $confirm_key;
          $email_subject = str_replace("%username%", $username, translation("No-Reply: User Registration Verification"));
          $email_message = str_replace("%username%", $username, CONFIRM_EMAIL_CONTENTS);
          $email_message = str_replace("%confirm_url%", $confirm_url, $email_message);
          $sendMail = sendEmail($email, $email_subject, $email_message);
          if (!$sendMail)
          {
            $success = false;
            $message[] = translation("Attempt to send a confirmation e-mail has failed. Registration process has been aborted.");
            delete_confirm_record($username, $email);
          }
          else
          {
            $attempt = add_user_referral($ref_id, $username);
            header("Location: " . $_SERVER['PHP_SELF'] . "?action=verify-email");
            exit();
          }
        }
      }
    }
    $out .= "<div class=\"ink-alert basic error\">";
    $out .= "<button class=\"ink-dismiss\">&times;</button>";
    foreach ($message as $text)
    {
      $out .= $text . "<br/>";
    }
    $out .= "</div>";
  }

  $out .= "<center><form method=\"post\" class=\"ink-form registration_form\">";
  $out .= "<table border=\"0\">";
  $out .= "<tr>";
  $out .= "<td class=\"form_labels\">" . translation("Username") . "</td>";
  $out .= "<td><input type=\"text\" name=\"username\" maxlength=\"12\" value=\"" . $username . "\">";
  $out .= "<p class=\"tip\">" . translation("Choose a username from 4 to 12 characters") . "</p>";
  $out .= "</td>";
  $out .= "</tr>";
  $out .= "<tr>";
  $out .= "<td class=\"form_labels\">" . translation("Email Address") . "</td>";
  $out .= "<td><input type=\"email\" name=\"email\" value=\"" . $email . "\">";
  $out .= "<p class=\"tip\">" . translation("Enter a valid e-mail address. E-mail verification is required") . "</p>";
  $out .= "</td>";
  $out .= "</tr>";
  $out .= "<tr>";
  $out .= "<td class=\"form_labels\">" . translation("Confirm Email") . "</td>";
  $out .= "<td><input type=\"email\" name=\"confirm_email\" value=\"\">";
  $out .= "<p class=\"tip\">" . translation("Re-type your e-mail address") . "</p>";
  $out .= "</td>";
  $out .= "</tr>";

  if (!$confirm_email_required)
  {
    $out .= "<tr>";
    $out .= "<td class=\"form_labels\">" . translation("Password") . "</td>";
    $out .= "<td><input type=\"password\" name=\"password\" maxlength=\"12\" value=\"\">";
    $out .= "<p class=\"tip\">" . translation("Between 4 and 12 characters. Can contain letters, numbers, and special characters(<b>!@#$%^&*</b>)") . "</p>";
    $out .= "</td>";
    $out .= "</tr>";
    $out .= "<tr>";
    $out .= "<td class=\"form_labels\">" . translation("Confirm Password") . "</td>";
    $out .= "<td><input type=\"password\" name=\"confirm_password\" maxlength=\"12\" value=\"\">";
    $out .= "<p class=\"tip\">" . translation("Re-type your Password") . "</p>";
    $out .= "</td>";
    $out .= "</tr>";
  }

  $out .= "<tr>";
  $out .= "<td class=\"form_submit\" colospan=2><button type=\"submit\" class=\"ink-button\" name=\"register\">" . translation("Register") . "</button></td>";
  $out .= "</tr>";
  $out .= "</table>";
  $out .= "</div>";
  $out .= "</fieldset>";
  $out .= "</form></center>";
}
else if ($action == "verify-email")
{
  $out .= "<h4>" . translation("E-Mail Verification Required") . "</h4>";
  $out .= "<div class=\"ink-alert basic success\">";
  $out .= "<center><p>" . translation("An e-mail has been sent to your account with a link to confirm your account. Please click the link to finish your registration and choose a password.") . "</p></center><br/>";
  $out .= "<center><p>" . translation("Thank You") . "</p></center>";
  $out .= "</div>";
}
else if ($action == "verify")
{
  $confirm = isset($_POST['confirm']) ? true : false;
  $confirm_key = isset($_GET['confirm_key']) ? antiject(trim($_GET['confirm_key'])) : "";
  $out .= "<center><h4>" . translation("Confirm E-Mail/Choose Password") . "</h4></center><br/>";
  $out .= "<center><p>" . translation("This page will confirm your account and also allow you to set a password for your account. Please fill out the form below. If your confirmation key matches you will successfully complete the registration process") . "</p></center><br/>";
  if ($confirm === true)
  {
    $db_confirm_key = "";
    $db_email = "";
    $success = true;
    $message = array ();
    if ($confirm_key == "" || $username == "" || $password == "")
    {
      $success = false;
      $message[] = translation("Some fields were left blank. All fields must be filled");
    }
    if (!preg_match(REGEX_USERNAME, $username))
    {
      $success = false;
      $message[] = translation("Invalid username provided. Username can only contain letters and numbers.");
    }
    if (strlen($username) < 4 || 12 < strlen($username))
    {
      $success = false;
      $message[] = translation("Username must be between 4 to 12 characters in length.");
    }
    if ($password != $confirm_password)
    {
      $success = false;
      $message[] = translation("Confirmation password does not match. Please check for typos.");
    }
    else if (!preg_match(REGEX_PASSWORD, $password))
    {
      $success = false;
      $message[] = translation("Invalid password provided. Password can only contain letters and numbers.");
    }
    if (strlen($password) < 4 || 12 < strlen($password))
    {
      $success = false;
      $message[] = translation("Password must be between 4 to 12 characters in length.");
    }
    if ($success === true)
    {
      if (confirmed_user_exists($username))
      {
        $success = false;
        $message[] = translation("This is odd. The username you are trying to confirm has already been confirmed. Please contact an administrator.");
      }
    }
    if ($success == true)
    {
      $confirm_attempt = validate_confirm_key($username, $confirm_key);
      if ($confirm_attempt["error"] === True)
      {
        $success = False;
        $message[] = translation($confirm_attempt["errorMessage"]);
      }
      else
      {
        $register_attempt = add_new_user($username, $password, $confirm_attempt["dbemail"], $user_ip, $securityLogin);

        if ($register_attempt["error"] === False)
        {
          delete_confirm_record($username, $confirm_attempt["dbemail"]);
          header("Location: " . $_SERVER['PHP_SELF'] . "?action=success");
          exit();
        }
        else
        {
          $success = False;
          $message[] = translation($register_attempt["error"]);
        }
      }
    }

    $out .= "<div class=\"ink-alert basic error\">";
    $out .= "<button class=\"ink-dismiss\">&times;</button>";
    foreach ($message as $text)
    {
      $out .= $text . "<br/>";
    }
    $out .= "</div>";
  }

  $out .= "<center><form method=\"post\" action=\"?action=verify&confirm_key=" . $confirm_key . "\" class=\"ink-form registration_form\">";
  $out .= "<table border=\"0\">";
  $out .= "<tr>";
  $out .= "<td class=\"form_labels\">" . translation("Confirm Key") . "</td>";
  $out .= "<td><input type=\"text\" disabled=\"disabled\" value=\"" . $confirm_key . "\">";
  $out .= "<p class=\"tip\">" . translation("This is the confirmation key sent to your e-mail.") . "</p>";
  $out .= "</td>";
  $out .= "</tr>";
  $out .= "<tr>";
  $out .= "<td class=\"form_labels\">" . translation("Confirm user") . "</td>";
  $out .= "<td><input type=\"text\" name=\"username\" maxlength=\"12\" value=\"" . $username . "\">";
  $out .= "<p class=\"tip\">" . translation("Enter the username you chose at registration.") . "</p>";
  $out .= "</td>";
  $out .= "</tr>";
  $out .= "<tr>";
  $out .= "<td class=\"form_labels\">" . translation("Password") . "</td>";
  $out .= "<td><input type=\"password\" name=\"password\" maxlength=\"12\" value=\"\">";
  $out .= "<p class=\"tip\">" . translation("Between 4 and 12 characters. Can contain letters, numbers, and special characters(<b>!@#$%^&*</b>)") . "</p>";
  $out .= "</td>";
  $out .= "</tr>";
  $out .= "<tr>";
  $out .= "<td class=\"form_labels\">" . translation("Confirm Password") . "</td>";
  $out .= "<td><input type=\"password\" name=\"confirm_password\" maxlength=\"12\" value=\"\">";
  $out .= "<p class=\"tip\">" . translation("Re-type your Password") . "</p>";
  $out .= "</td>";
  $out .= "</tr>";
  $out .= "<tr>";
  $out .= "<td class=\"form_submit\" colospan=2><button type=\"submit\" class=\"ink-button\" name=\"confirm\">" . translation("Confirm Account") . "</button></td>";
  $out .= "</tr>";
  $out .= "</table>";
  $out .= "</div>";
  $out .= "</fieldset>";
  $out .= "</form></center>";
}
else if ($action == "success")
{
  $out .= "<h4>" . translation("Success! You have completed the registration process") . "</h4>";
  $out .= "<div class=\"ink-alert basic success\">";
  $out .= "<center><p>" . translation("Your registration is now complete. You must log in game before logging into the RF Control Panel.") . "</p></center><br/>";
  $out .= "<center><p>" . translation("Thank You!") . "</p></center>";
  $out .= "</div>";
}
else
{
  $out .= translation("invalid_action");
}
gamecp_nav();
$output = gamecp_template("gamecp");
print_outputs($output);
