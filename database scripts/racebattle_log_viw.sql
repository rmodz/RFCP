USE [RF_WORLD]
GO

/****** Object:  View [dbo].[racebattle_log_view]    Script Date: 06/06/2014 15:02:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[racebattle_log_view]
AS
SELECT [idx]
      ,[worldname]
      ,[szdate]
      ,[nth]
      ,[endtime]
      ,[winrace]
      ,[loserace]
      ,[regdate]
      ,[bossserial0]
      ,[bossserial1]
      ,[bossserial2]
	  ,CASE 
		WHEN CAST(RIGHT(CAST(endtime as varchar),4) AS INT) < 100 AND nth = 3 THEN
		DATEADD(DAY, -1, (CAST((szdate + ' 22:00:00') as DATETIME)))
		WHEN nth = 3 THEN
		CAST((szdate + ' 22:00:00') as DATETIME)
		WHEN nth = 2 THEN
		CAST((szdate + ' 14:00:00') as DATETIME)
		WHEN nth = 1 THEN
		CAST((szdate + ' 06:00:00') as DATETIME)
	    END AS starttime
	  ,CASE 
		WHEN CAST(RIGHT(CAST(endtime as varchar),4) AS INT) < 100 AND nth = 3 THEN
		DATEDIFF(SECOND, DATEADD(DAY, -1, (CAST((szdate + ' 22:00:00') as DATETIME))), regdate)
		WHEN nth = 3 THEN
		DATEDIFF(SECOND, CAST((szdate + ' 22:00:00') as DATETIME), regdate)
		WHEN nth = 2 THEN
		DATEDIFF(SECOND, CAST((szdate + ' 14:00:00') as DATETIME), regdate)
		WHEN nth = 1 THEN
		DATEDIFF(SECOND, CAST((szdate + ' 06:00:00') as DATETIME), regdate)
	    END AS time_elapsed
  FROM [RF_WORLD].[dbo].[tbl_racebattle_log]
GO

