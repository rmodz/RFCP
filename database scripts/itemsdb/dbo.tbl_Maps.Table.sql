USE [RF_ItemsDB]
GO
/****** Object:  Table [dbo].[tbl_Maps]    Script Date: 06/05/2014 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Maps](
	[map_id] [int] NOT NULL,
	[map_name] [varchar](255) NULL,
	[map_ui_name] [varchar](255) NULL,
 CONSTRAINT [PK_tbl_Maps] PRIMARY KEY CLUSTERED 
(
	[map_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (0, N'NeutralB', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (1, N'NeutralC', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (2, N'resources', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (3, N'NeutralA', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (4, N'NeutralBS1', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (5, N'NeutralBS2', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (6, N'NeutralCS1', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (7, N'NeutralCS2', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (8, N'NeutralAS1', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (9, N'NeutralAS2', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (10, N'Platform01', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (11, N'Sette', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (12, N'Cauldron01', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (13, N'Elan', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (14, N'Dungeon00', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (15, N'Transport01', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (16, N'Dungeon01', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (17, N'AccGSD', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (18, N'BellaGSD', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (19, N'CoraGSD', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (20, N'AccGSP', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (21, N'BellaGSP', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (22, N'CoraGSP', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (23, N'Dungeon02', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (24, N'Exile_Land', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (25, N'Mountain_Beast', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (26, N'Medicallab', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (27, N'Cora', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (28, N'Dungeon03', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (29, N'Medicallab2', NULL)
INSERT [dbo].[tbl_Maps] ([map_id], [map_name], [map_ui_name]) VALUES (30, N'Dungeon04', NULL)
