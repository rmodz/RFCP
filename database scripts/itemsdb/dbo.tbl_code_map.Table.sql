USE [RF_ItemsDB]
GO
/****** Object:  Table [dbo].[tbl_code_map]    Script Date: 06/05/2014 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_code_map](
	[item_id] [int] NOT NULL,
	[item_code] [varchar](50) NULL,
	[item_name] [varchar](255) NULL,
	[item_client_id] [int] NULL,
	[item_icon_id] [int] NULL,
	[item_level] [int] NULL,
	[item_min_attack] [int] NULL,
	[item_max_attack] [int] NULL,
	[item_adr] [float] NULL,
	[item_adp] [float] NULL,
	[item_dsr] [float] NULL,
	[ability_desc] [varchar](255) NULL,
 CONSTRAINT [PK_tbl_code_map] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (0, N'izbbb00', N'Training Bellato HQ(Map)', -1056898816, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (1, N'izccc00', N'Training Cora HQ(Map)', -1040055808, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (2, N'izaaa00', N'Training Accretia HQ(Map)', -1073741824, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (3, N'izbbb01', N'Bellato HQ(Map)', -1056898815, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (4, N'izccc01', N'Cora HQ(Map)', -1040055807, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (5, N'izaaa01', N'Accretia HQ(Map)', -1073741823, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (6, N'izbnn01', N'Solus Settlement(Map)', -1056109311, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (7, N'izbnn02', N'Anacaade Settlement(Map)', -1056109310, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (8, N'izcnn01', N'Haram Stockade(Map)', -1039332095, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (9, N'izcnn02', N'Numerus Stockade(Map)', -1039332094, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (10, N'izann01', N'Armory-213(Map)', -1072886527, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (11, N'izann02', N'Armory-117(Map)', -1072886526, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (12, N'izree01', N'Crag Mine Field(Map)', -788265983, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (13, N'izpll01', N'Ether Platform(Map)', -821359871, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (14, N'izset01', N'Sette Desert(Map)', -771484927, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (15, N'izcol01', N'Caldron Volcanic Zone(Map)', -1039267071, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (16, N'izelf00', N'Ancient Edisil Kingdom`s Map(Map)', -1005910784, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_map] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (17, N'izelf01', N'Elven Area(Map)', -1005910783, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
