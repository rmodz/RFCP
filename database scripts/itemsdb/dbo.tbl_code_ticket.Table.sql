USE [RF_ItemsDB]
GO
/****** Object:  Table [dbo].[tbl_code_ticket]    Script Date: 06/05/2014 21:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_code_ticket](
	[item_id] [int] NOT NULL,
	[item_code] [varchar](50) NULL,
	[item_name] [varchar](255) NULL,
	[item_client_id] [int] NULL,
	[item_icon_id] [int] NULL,
	[item_level] [int] NULL,
	[item_min_attack] [int] NULL,
	[item_max_attack] [int] NULL,
	[item_adr] [float] NULL,
	[item_adp] [float] NULL,
	[item_dsr] [float] NULL,
	[ability_desc] [varchar](255) NULL,
 CONSTRAINT [PK_tbl_code_ticket] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (0, N'tibp000', N'Cartella Air Transpo Ticket: Bellato HQ to Ether Platfrom', 1896808448, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (1, N'ticp000', N'Cartella Air Transpo Ticket: Cora HQ to Ether Platfrom', 1913585664, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (2, N'tiap000', N'Cartella Air Transpo Ticket: Accretia HQ to Ether Platform', 1880031232, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (3, N'tipb000', N'Cartella Air Transpo Ticket: Ether Platform to Bellato HQ', 2130771968, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (4, N'tipc000', N'Cartella Air Transpo Ticket: Ether Platform to Cora HQ', 2130837504, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (5, N'tipa000', N'Cartella Air Transpo Ticket: Ether Platform to Accretia HQ', 2130706432, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (6, N'tibp001', N'Cartella Air Transpo Ticket: Bellato HQ to Ether Platfrom', 1896808704, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (7, N'ticp001', N'Cartella Air Transpo Ticket: Cora HQ to Ether Platfrom', 1913585920, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (8, N'tiap001', N'Cartella Air Transpo Ticket: Accretia HQ to Ether Platform', 1880031488, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (9, N'tipb001', N'Cartella Air Transpo Ticket: Ether Platform to Bellato HQ', 2130772224, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (10, N'tipc001', N'Cartella Air Transpo Ticket: Ether Platform to Cora HQ', 2130837760, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbl_code_ticket] ([item_id], [item_code], [item_name], [item_client_id], [item_icon_id], [item_level], [item_min_attack], [item_max_attack], [item_adr], [item_adp], [item_dsr], [ability_desc]) VALUES (11, N'tipa001', N'Cartella Air Transpo Ticket: Ether Platform to Accretia HQ', 2130706688, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
