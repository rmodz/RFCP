USE [RF_GAMECP]
GO

/****** Object:  Table [dbo].[gamecp_g_market_items]    Script Date: 01/12/2017 23:23:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[gamecp_g_market_items](
	[item_id] [int] IDENTITY(1,1) NOT NULL,
	[item_status] [int] NOT NULL,
	[item_cat_id] [int] NOT NULL,
	[item_date_added] [int] NULL,
	[item_date_updated] [int] NULL,
	[item_race] [int] NOT NULL,
	[item_price] [real] NOT NULL,
	[item_dbcode] [int] NULL,
	[item_amount] [bigint] NOT NULL,
	[item_custom_amount] [int] NOT NULL,
	[item_upgrade] [int] NULL,
	[item_name] [varchar](255) NULL,
	[item_description] [varchar](255) NULL,
	[item_image_url] [varchar](255) NOT NULL,
	[item_buy_count] [int] NOT NULL,
	[item_delete] [int] NOT NULL,
	[item_rent_time] [int] NOT NULL,
 CONSTRAINT [PK_gamecp_g_shop_items] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[gamecp_g_market_items] ADD  CONSTRAINT [DF_gamecp_g_shop_items_item_status]  DEFAULT ((1)) FOR [item_status]
GO

ALTER TABLE [dbo].[gamecp_g_market_items] ADD  CONSTRAINT [DF_gamecp_g_shop_items_item_cat_id]  DEFAULT ((0)) FOR [item_cat_id]
GO

ALTER TABLE [dbo].[gamecp_g_market_items] ADD  CONSTRAINT [DF_gamecp_g_shop_items_item_race]  DEFAULT ((0)) FOR [item_race]
GO

ALTER TABLE [dbo].[gamecp_g_market_items] ADD  CONSTRAINT [DF_gamecp_g_shop_items_item_price]  DEFAULT ((0)) FOR [item_price]
GO

ALTER TABLE [dbo].[gamecp_g_market_items] ADD  CONSTRAINT [DF_gamecp_g_shop_items_item_custom_amount]  DEFAULT ((0)) FOR [item_custom_amount]
GO

ALTER TABLE [dbo].[gamecp_g_market_items] ADD  CONSTRAINT [DF_gamecp_g_shop_items_item_upgrade_talic]  DEFAULT ((0)) FOR [item_upgrade]
GO

ALTER TABLE [dbo].[gamecp_g_market_items] ADD  CONSTRAINT [DF_gamecp_g_shop_items_item_download_count]  DEFAULT ((0)) FOR [item_buy_count]
GO

ALTER TABLE [dbo].[gamecp_g_market_items] ADD  CONSTRAINT [DF_gamecp_g_shop_items_item_delete]  DEFAULT ((0)) FOR [item_delete]
GO

ALTER TABLE [dbo].[gamecp_g_market_items] ADD  CONSTRAINT [rent_g_time_default]  DEFAULT ((0)) FOR [item_rent_time]
GO


