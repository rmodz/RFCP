
ALTER DATABASE [RF_GAMECP] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RF_GAMECP].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RF_GAMECP] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [RF_GAMECP] SET ANSI_NULLS OFF
GO
ALTER DATABASE [RF_GAMECP] SET ANSI_PADDING OFF
GO
ALTER DATABASE [RF_GAMECP] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [RF_GAMECP] SET ARITHABORT OFF
GO
ALTER DATABASE [RF_GAMECP] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [RF_GAMECP] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [RF_GAMECP] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [RF_GAMECP] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [RF_GAMECP] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [RF_GAMECP] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [RF_GAMECP] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [RF_GAMECP] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [RF_GAMECP] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [RF_GAMECP] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [RF_GAMECP] SET  DISABLE_BROKER
GO
ALTER DATABASE [RF_GAMECP] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [RF_GAMECP] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [RF_GAMECP] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [RF_GAMECP] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [RF_GAMECP] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [RF_GAMECP] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [RF_GAMECP] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [RF_GAMECP] SET  READ_WRITE
GO
ALTER DATABASE [RF_GAMECP] SET RECOVERY FULL
GO
ALTER DATABASE [RF_GAMECP] SET  MULTI_USER
GO
ALTER DATABASE [RF_GAMECP] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [RF_GAMECP] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'RF_GAMECP', N'ON'
GO
USE [RF_GAMECP]
GO
/****** Object:  User [gamecp]    Script Date: 06/05/2014 21:40:18 ******/
CREATE USER [gamecp] FOR LOGIN [gamecp] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[ticket_categories]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ticket_categories](
	[id] [int] NOT NULL,
	[category] [nvarchar](40) NOT NULL,
	[permission] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gamecp_vote_sites]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_vote_sites](
	[vote_id] [int] IDENTITY(1,1) NOT NULL,
	[vote_site_name] [varchar](255) NOT NULL,
	[vote_site_url] [varchar](255) NOT NULL,
	[vote_site_image] [varchar](255) NOT NULL,
	[vote_reset_time] [int] NULL,
	[vote_count] [int] NULL,
 CONSTRAINT [PK_gamecp_vote_sites] PRIMARY KEY CLUSTERED 
(
	[vote_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_vote_log]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_vote_log](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[log_account_serial] [int] NOT NULL,
	[log_time] [varchar](255) NOT NULL,
	[log_ip] [varchar](255) NULL,
	[log_points_gained] [real] NULL,
	[log_total_points] [real] NULL,
	[site_id] [int] NOT NULL,
 CONSTRAINT [PK_gamecp_vote_log] PRIMARY KEY CLUSTERED 
(
	[log_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_username_log]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_username_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username_charid] [int] NOT NULL,
	[username_oldname] [varchar](255) NULL,
	[username_newname] [varchar](255) NULL,
	[username_ip] [varchar](255) NULL,
 CONSTRAINT [PK_username_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_ticket_entry]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gamecp_ticket_entry](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NOT NULL,
	[type] [nvarchar](16) NOT NULL,
	[account] [nvarchar](16) NOT NULL,
	[title] [nvarchar](50) NOT NULL,
	[status] [nvarchar](16) NOT NULL,
 CONSTRAINT [PK_gamecp_ticket_entry] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gamecp_ticket_data]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gamecp_ticket_data](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[entryid] [int] NOT NULL,
	[isgm] [int] NOT NULL,
	[name] [nvarchar](16) NOT NULL,
	[ticket] [nvarchar](max) NOT NULL,
	[replyid] [int] NOT NULL,
	[date] [datetime] NOT NULL,
 CONSTRAINT [PK_gamecp_ticket_data] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gamecp_shop_categories]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_shop_categories](
	[cat_id] [int] IDENTITY(1,1) NOT NULL,
	[cat_sub_id] [int] NULL,
	[cat_order] [int] NULL,
	[cat_name] [varchar](255) NULL,
	[cat_description] [varchar](255) NULL,
 CONSTRAINT [PK_gamecp_shop_category] PRIMARY KEY CLUSTERED 
(
	[cat_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_sessions]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_sessions](
	[session_id] [int] IDENTITY(1,1) NOT NULL,
	[session_username] [varchar](255) NOT NULL,
	[session_ip] [varchar](255) NOT NULL,
	[session_account_serial] [int] NOT NULL,
	[session_created_time] [int] NOT NULL,
	[session_guid] [varchar](38) NOT NULL,
	[session_refreshed_time] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_rented_items]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_rented_items](
	[rented_id] [int] IDENTITY(1,1) NOT NULL,
	[rented_name] [varchar](255) NOT NULL,
	[rented_desc] [varchar](max) NULL,
	[rented_k] [int] NOT NULL,
	[rented_u] [int] NOT NULL,
	[rented_d] [int] NOT NULL,
	[rented_time] [int] NOT NULL,
	[rented_price] [float] NOT NULL,
	[rented_custom_amount] [int] NOT NULL,
 CONSTRAINT [PK_gamecp_rented_items] PRIMARY KEY CLUSTERED 
(
	[rented_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_registration_log]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_registration_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[reg_account] [varchar](255) NULL,
	[reg_ip] [varchar](255) NULL,
	[reg_time] [varchar](255) NULL,
	[reg_browser] [varchar](255) NULL,
 CONSTRAINT [PK_userreg_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_redeem_log]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_redeem_log](
	[redeem_id] [int] IDENTITY(1,1) NOT NULL,
	[redeem_account_id] [int] NOT NULL,
	[redeem_char_id] [int] NOT NULL,
	[redeem_price] [real] NOT NULL,
	[redeem_total_gp] [real] NOT NULL,
	[redeem_item_id] [int] NOT NULL,
	[redeem_item_name] [varchar](255) NULL,
	[redeem_item_dbcode] [bigint] NULL,
	[redeem_item_amount] [bigint] NOT NULL,
	[redeem_time] [varchar](255) NULL,
 CONSTRAINT [PK_gamecp_redeem_log] PRIMARY KEY CLUSTERED 
(
	[redeem_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_permissions]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_permissions](
	[admin_id] [int] IDENTITY(1,1) NOT NULL,
	[admin_serial] [int] NOT NULL,
	[admin_permission] [varchar](max) NOT NULL,
	[admin_custom_permissions] [varchar](max) NULL,
	[admin_display_name] [varchar](256) NULL,
 CONSTRAINT [PK_gamecp_permissions] PRIMARY KEY CLUSTERED 
(
	[admin_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_paypal]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_paypal](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tranid] [varchar](50) NULL,
	[amount] [nvarchar](50) NULL,
	[fee] [varchar](50) NULL,
	[userid] [varchar](50) NULL,
	[name] [nvarchar](30) NULL,
	[credits] [real] NULL,
	[time] [varchar](50) NULL,
	[payerid] [varchar](50) NULL,
	[payeremail] [varchar](50) NULL,
	[verified] [int] NULL,
	[dupetran] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_market_items]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_market_items](
	[item_id] [int] IDENTITY(1,1) NOT NULL,
	[item_status] [int] NOT NULL,
	[item_cat_id] [int] NOT NULL,
	[item_date_added] [int] NULL,
	[item_date_updated] [int] NULL,
	[item_race] [int] NOT NULL,
	[item_price] [real] NOT NULL,
	[item_dbcode] [int] NULL,
	[item_amount] [bigint] NOT NULL,
	[item_custom_amount] [int] NOT NULL,
	[item_upgrade] [int] NULL,
	[item_name] [varchar](255) NULL,
	[item_description] [varchar](255) NULL,
	[item_image_url] [varchar](255) NOT NULL,
	[item_buy_count] [int] NOT NULL,
	[item_delete] [int] NOT NULL,
	[item_rent_time] [int] NOT NULL,
 CONSTRAINT [PK_gamecp_shop_items] PRIMARY KEY CLUSTERED 
(
	[item_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_log]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[log_level] [int] NULL,
	[log_time] [int] NULL,
	[log_account] [varchar](255) NULL,
	[log_message] [varchar](255) NULL,
	[log_ip] [varchar](255) NULL,
	[log_page] [varchar](255) NULL,
	[log_browser] [varchar](255) NULL,
 CONSTRAINT [PK_gamecp_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_gamecp_log] UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_google_payments]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_google_payments](
	[google_id] [int] IDENTITY(1,1) NOT NULL,
	[google_order_id] [bigint] NOT NULL,
	[google_order_price] [real] NOT NULL,
	[google_order_points] [real] NULL,
	[google_order_state] [int] NOT NULL,
	[google_buyer_id] [bigint] NOT NULL,
	[google_buyer_email] [varchar](255) NOT NULL,
	[google_buyer_ip] [varchar](255) NULL,
	[google_buyer_name] [varchar](255) NULL,
	[google_buyer_address] [varchar](255) NULL,
	[google_buyer_postal_code] [varchar](255) NULL,
	[google_buyer_account_age] [int] NULL,
	[google_account_serial] [int] NOT NULL,
	[google_time] [int] NOT NULL,
 CONSTRAINT [PK_gamecp_google_payments] PRIMARY KEY CLUSTERED 
(
	[google_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_gamecp_google_payments] UNIQUE NONCLUSTERED 
(
	[google_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_gamecp_google_payments_1] UNIQUE NONCLUSTERED 
(
	[google_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_gamepoints]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_gamepoints](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[user_account_id] [int] NULL,
	[user_points] [float] NULL,
	[user_vote_points] [real] NULL,
  [user_guild_points] [float] NULL,
	[user_vote_timestamp] [varchar](255) NULL,
 CONSTRAINT [PK_gamecp_gamepoints] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_gamecp_gamepoints] UNIQUE NONCLUSTERED 
(
	[user_account_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_confirm_email]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_confirm_email](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[confirm_key] [varchar](36) NOT NULL,
	[username] [varchar](16) NOT NULL,
	[email] [varchar](255) NOT NULL,
	[created_at] [int] NOT NULL,
 CONSTRAINT [PK_gamecp_confirm_email] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gamecp_config]    Script Date: 06/05/2014 21:40:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gamecp_config](
	[config_name] [varchar](255) NOT NULL,
	[config_value] [varchar](255) NULL,
	[config_description] [varchar](255) NULL,
 CONSTRAINT [PK_gamecp_config] PRIMARY KEY CLUSTERED 
(
	[config_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF_gamecp_vote_sites_vote_reset_time]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_vote_sites] ADD  CONSTRAINT [DF_gamecp_vote_sites_vote_reset_time]  DEFAULT ((43200)) FOR [vote_reset_time]
GO
/****** Object:  Default [DF_gamecp_vote_sites_vote_count]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_vote_sites] ADD  CONSTRAINT [DF_gamecp_vote_sites_vote_count]  DEFAULT ((0)) FOR [vote_count]
GO
/****** Object:  Default [DF_gamecp_vote_log_log_points_gained]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_vote_log] ADD  CONSTRAINT [DF_gamecp_vote_log_log_points_gained]  DEFAULT ((0)) FOR [log_points_gained]
GO
/****** Object:  Default [DF_gamecp_vote_log_log_total_points_before]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_vote_log] ADD  CONSTRAINT [DF_gamecp_vote_log_log_total_points_before]  DEFAULT ((0)) FOR [log_total_points]
GO
/****** Object:  Default [DF__gamecp_tic__date__49C3F6B7]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_ticket_entry] ADD  DEFAULT (getdate()) FOR [date]
GO
/****** Object:  Default [DF_gamecp_ticket_data_replyid]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_ticket_data] ADD  CONSTRAINT [DF_gamecp_ticket_data_replyid]  DEFAULT ((0)) FOR [replyid]
GO
/****** Object:  Default [DF__gamecp_tic__date__4AB81AF0]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_ticket_data] ADD  DEFAULT (getdate()) FOR [date]
GO
/****** Object:  Default [DF_gamecp_shop_categories_cat_sub_id]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_shop_categories] ADD  CONSTRAINT [DF_gamecp_shop_categories_cat_sub_id]  DEFAULT ((0)) FOR [cat_sub_id]
GO
/****** Object:  Default [DF_gamecp_shop_categories_cat_order]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_shop_categories] ADD  CONSTRAINT [DF_gamecp_shop_categories_cat_order]  DEFAULT ((0)) FOR [cat_order]
GO
/****** Object:  Default [DF_gamecp_rented_items_rented_k]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_rented_items] ADD  CONSTRAINT [DF_gamecp_rented_items_rented_k]  DEFAULT ((-1)) FOR [rented_k]
GO
/****** Object:  Default [DF_gamecp_rented_items_rented_u]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_rented_items] ADD  CONSTRAINT [DF_gamecp_rented_items_rented_u]  DEFAULT ((268435455)) FOR [rented_u]
GO
/****** Object:  Default [DF_gamecp_rented_items_rented_d]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_rented_items] ADD  CONSTRAINT [DF_gamecp_rented_items_rented_d]  DEFAULT ((1)) FOR [rented_d]
GO
/****** Object:  Default [DF_gamecp_rented_items_rented_time]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_rented_items] ADD  CONSTRAINT [DF_gamecp_rented_items_rented_time]  DEFAULT ((86400)) FOR [rented_time]
GO
/****** Object:  Default [DF_gamecp_rented_items_rented_price]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_rented_items] ADD  CONSTRAINT [DF_gamecp_rented_items_rented_price]  DEFAULT ((0)) FOR [rented_price]
GO
/****** Object:  Default [DF_gamecp_rented_items_rented_custom_amount]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_rented_items] ADD  CONSTRAINT [DF_gamecp_rented_items_rented_custom_amount]  DEFAULT ((1)) FOR [rented_custom_amount]
GO
/****** Object:  Default [DF_gamecp_redeem_log_redeem_price]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_redeem_log] ADD  CONSTRAINT [DF_gamecp_redeem_log_redeem_price]  DEFAULT ((0)) FOR [redeem_price]
GO
/****** Object:  Default [DF_gamecp_redeem_log_redeem_total_gp]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_redeem_log] ADD  CONSTRAINT [DF_gamecp_redeem_log_redeem_total_gp]  DEFAULT ((0)) FOR [redeem_total_gp]
GO
/****** Object:  Default [DF_gamecp_redeem_log_redeem_item_amount]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_redeem_log] ADD  CONSTRAINT [DF_gamecp_redeem_log_redeem_item_amount]  DEFAULT ((0)) FOR [redeem_item_amount]
GO
/****** Object:  Default [DF_gamecp_permissions_admin_permission]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_permissions] ADD  CONSTRAINT [DF_gamecp_permissions_admin_permission]  DEFAULT ((0)) FOR [admin_permission]
GO
/****** Object:  Default [DF__gamecp_pe__admin__4BAC3F29]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_permissions] ADD  DEFAULT ('') FOR [admin_custom_permissions]
GO
/****** Object:  Default [DF__gamecp_pe__admin__4CA06362]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_permissions] ADD  DEFAULT ('') FOR [admin_display_name]
GO
/****** Object:  Default [DF_gamecp_shop_items_item_status]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_market_items] ADD  CONSTRAINT [DF_gamecp_shop_items_item_status]  DEFAULT ((1)) FOR [item_status]
GO
/****** Object:  Default [DF_gamecp_shop_items_item_cat_id]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_market_items] ADD  CONSTRAINT [DF_gamecp_shop_items_item_cat_id]  DEFAULT ((0)) FOR [item_cat_id]
GO
/****** Object:  Default [DF_gamecp_shop_items_item_race]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_market_items] ADD  CONSTRAINT [DF_gamecp_shop_items_item_race]  DEFAULT ((0)) FOR [item_race]
GO
/****** Object:  Default [DF_gamecp_shop_items_item_price]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_market_items] ADD  CONSTRAINT [DF_gamecp_shop_items_item_price]  DEFAULT ((0)) FOR [item_price]
GO
/****** Object:  Default [DF_gamecp_shop_items_item_custom_amount]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_market_items] ADD  CONSTRAINT [DF_gamecp_shop_items_item_custom_amount]  DEFAULT ((0)) FOR [item_custom_amount]
GO
/****** Object:  Default [DF_gamecp_shop_items_item_upgrade_talic]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_market_items] ADD  CONSTRAINT [DF_gamecp_shop_items_item_upgrade_talic]  DEFAULT ((0)) FOR [item_upgrade]
GO
/****** Object:  Default [DF_gamecp_shop_items_item_download_count]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_market_items] ADD  CONSTRAINT [DF_gamecp_shop_items_item_download_count]  DEFAULT ((0)) FOR [item_buy_count]
GO
/****** Object:  Default [DF_gamecp_shop_items_item_delete]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_market_items] ADD  CONSTRAINT [DF_gamecp_shop_items_item_delete]  DEFAULT ((0)) FOR [item_delete]
GO
/****** Object:  Default [rent_time_default]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_market_items] ADD  CONSTRAINT [rent_time_default]  DEFAULT ((0)) FOR [item_rent_time]
GO
/****** Object:  Default [DF_gamecp_log_log_level]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_log] ADD  CONSTRAINT [DF_gamecp_log_log_level]  DEFAULT ((0)) FOR [log_level]
GO
/****** Object:  Default [DF_gamecp_google_payments_google_order_state]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_google_payments] ADD  CONSTRAINT [DF_gamecp_google_payments_google_order_state]  DEFAULT ((0)) FOR [google_order_state]
GO
/****** Object:  Default [DF_gamecp_gamepoints_user_points]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_gamepoints] ADD  CONSTRAINT [DF_gamecp_gamepoints_user_points]  DEFAULT ((0)) FOR [user_points]
GO
/****** Object:  Default [DF_gamecp_gamepoints_user_vote_points]    Script Date: 06/05/2014 21:40:18 ******/
ALTER TABLE [dbo].[gamecp_gamepoints] ADD  CONSTRAINT [DF_gamecp_gamepoints_user_vote_points]  DEFAULT ((0)) FOR [user_vote_points]
GO
