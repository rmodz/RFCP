USE [RF_WORLD]
GO

/****** Object:  StoredProcedure [dbo].[pCheckInventory]    Script Date: 06/09/2014 05:43:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[pCheckInventory]
AS
BEGIN
	SET NOCOUNT ON;
	CREATE TABLE #Counts (StmtRowCount INT)
	CREATE TABLE #Serials (Serial INT)
	CREATE TABLE #AccountSerials (AccountSerial INT)
	DECLARE @Iterator INT
	DECLARE @MaxLastConnTime INT
	DECLARE @LastConnTimeUsed INT
	DECLARE @RecordsProcessed INT
	DECLARE @RecordsDeleted INT
	SET @Iterator = 0
	SET @RecordsDeleted = 0
	
	SELECT TOP 1 @MaxLastConnTime = LastConnTime FROM tbl_base ORDER BY LastConnTime DESC
	SELECT TOP 1 @LastConnTimeUsed = LastConnTime FROM RF_HISTORY.dbo.tbl_runs ORDER BY id DESC
	
	INSERT INTO #Serials
		SELECT Serial FROM tbl_base
		WHERE LastConnTime > @LastConnTimeUsed
	
	INSERT INTO #AccountSerials
		SELECT DISTINCT(AccountSerial) FROM tbl_base
		WHERE LastConnTime > @LastConnTimeUsed
		
	DELETE FROM RF_HISTORY.dbo.BELL_ItemLogs 
		WHERE (serial IN (SELECT Serial FROM #Serials) AND [table] IN ('tbl_base', 'tbl_inven', 'tbl_general'))
		OR (serial IN (SELECT AccountSerial FROM #AccountSerials) AND [table] IN ('tbl_AccountTrunk', 'tbl_AccountTrunk_Extend'))
	SET @RecordsDeleted = @RecordsDeleted + @@ROWCOUNT
	DELETE FROM RF_HISTORY.dbo.ACC_ItemLogs 
		WHERE (serial IN (SELECT Serial FROM #Serials) AND [table] IN ('tbl_base', 'tbl_inven', 'tbl_general'))
		OR (serial IN (SELECT AccountSerial FROM #AccountSerials) AND [table] IN ('tbl_AccountTrunk', 'tbl_AccountTrunk_Extend'))
	SET	@RecordsDeleted = @RecordsDeleted + @@ROWCOUNT
	DELETE FROM RF_HISTORY.dbo.CORA_ItemLogs 
		WHERE (serial IN (SELECT Serial FROM #Serials) AND [table] IN ('tbl_base', 'tbl_inven', 'tbl_general'))
		OR (serial IN (SELECT AccountSerial FROM #AccountSerials) AND [table] IN ('tbl_AccountTrunk', 'tbl_AccountTrunk_Extend'))
	SET @RecordsDeleted = @RecordsDeleted + @@ROWCOUNT
	
	WHILE (@Iterator < 8)
	BEGIN
		EXEC('INSERT INTO RF_HISTORY.dbo.BELL_ItemLogs
			SELECT src.Serial, EK' + @Iterator + ', -1, EU' + @Iterator + ', ET' + @Iterator + ', ES' + @Iterator + ', ''EK' + @Iterator + ''', ''tbl_base''
			FROM tbl_base src INNER JOIN #Serials s ON src.Serial = s.Serial
			WHERE src.Race IN (0,1) AND src.EK' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.ACC_ItemLogs
			SELECT src.Serial, EK' + @Iterator + ', -1, EU' + @Iterator + ', ET' + @Iterator + ', ES' + @Iterator + ', ''EK' + @Iterator + ''', ''tbl_base''
			FROM tbl_base src INNER JOIN #Serials s ON src.Serial = s.Serial
			WHERE src.Race IN (2,3) AND src.EK' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.CORA_ItemLogs
			SELECT src.Serial, EK' + @Iterator + ', -1, EU' + @Iterator + ', ET' + @Iterator + ', ES' + @Iterator + ', ''EK' + @Iterator + ''', ''tbl_base''
			FROM tbl_base src INNER JOIN #Serials s ON src.Serial = s.Serial
			WHERE src.Race = 4 AND src.EK' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		SET @Iterator = @Iterator + 1
	END
	
	SET @Iterator = 0
	
	WHILE (@Iterator < 100)
	BEGIN
		EXEC('INSERT INTO RF_HISTORY.dbo.BELL_ItemLogs
			SELECT src_base.Serial, K' + @Iterator + ', D' + @Iterator + ', U' + @Iterator + ', T' + @Iterator + ', S' + @Iterator + ', ''K' + @Iterator + ''', ''tbl_inven''
			FROM (tbl_base src_base JOIN tbl_inven src_inven ON src_base.Serial = src_inven.Serial)
			INNER JOIN #Serials s ON src_base.Serial = s.Serial 
			WHERE src_base.Race IN (0,1) AND src_inven.K' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.ACC_ItemLogs
			SELECT src_base.Serial, K' + @Iterator + ', D' + @Iterator + ', U' + @Iterator + ', T' + @Iterator + ', S' + @Iterator + ', ''K' + @Iterator + ''', ''tbl_inven''
			FROM (tbl_base src_base JOIN tbl_inven src_inven ON src_base.Serial = src_inven.Serial)
			INNER JOIN #Serials s ON src_base.Serial = s.Serial 
			WHERE src_base.Race IN (2,3) AND src_inven.K' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.CORA_ItemLogs
			SELECT src_base.Serial, K' + @Iterator + ', D' + @Iterator + ', U' + @Iterator + ', T' + @Iterator + ', S' + @Iterator + ', ''K' + @Iterator + ''', ''tbl_inven''
			FROM (tbl_base src_base JOIN tbl_inven src_inven ON src_base.Serial = src_inven.Serial)
			INNER JOIN #Serials s ON src_base.Serial = s.Serial 
			WHERE src_base.Race = 4 AND src_inven.K' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		SET @Iterator = @Iterator + 1
	END
	
	SET @Iterator = 0
	
	WHILE (@Iterator < 100)
	BEGIN
		EXEC('INSERT INTO RF_HISTORY.dbo.BELL_ItemLogs
			SELECT src.AccountSerial, K' + @Iterator + ', D' + @Iterator + ', U' + @Iterator + ', T' + @Iterator + ', S' + @Iterator + ', ''K' + @Iterator + ''', ''tbl_AccountTrunk''
			FROM tbl_AccountTrunk src INNER JOIN #AccountSerials s ON src.AccountSerial = s.AccountSerial
			WHERE src.R' + @Iterator + ' = 0 AND src.K' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.ACC_ItemLogs
			SELECT src.AccountSerial, K' + @Iterator + ', D' + @Iterator + ', U' + @Iterator + ', T' + @Iterator + ', S' + @Iterator + ', ''K' + @Iterator + ''', ''tbl_AccountTrunk''
			FROM tbl_AccountTrunk src INNER JOIN #AccountSerials s ON src.AccountSerial = s.AccountSerial
			WHERE src.R' + @Iterator + ' = 1 AND src.K' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.CORA_ItemLogs
			SELECT src.AccountSerial, K' + @Iterator + ', D' + @Iterator + ', U' + @Iterator + ', T' + @Iterator + ', S' + @Iterator + ', ''K' + @Iterator + ''', ''tbl_AccountTrunk''
			FROM tbl_AccountTrunk src INNER JOIN #AccountSerials s ON src.AccountSerial = s.AccountSerial
			WHERE src.R' + @Iterator + ' = 2 AND src.K' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		SET @Iterator = @Iterator + 1
	END
	
	SET @Iterator = 0
	
	WHILE (@Iterator < 40)
	BEGIN
		EXEC('INSERT INTO RF_HISTORY.dbo.BELL_ItemLogs
			SELECT src.AccountSerial, K' + @Iterator + ', D' + @Iterator + ', U' + @Iterator + ', T' + @Iterator + ', S' + @Iterator + ', ''K' + @Iterator + ''', ''tbl_AccountTrunk_Extend''
			FROM tbl_AccountTrunk_Extend src INNER JOIN #AccountSerials s ON src.AccountSerial = s.AccountSerial
			WHERE src.R' + @Iterator + ' = 0 AND src.K' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.ACC_ItemLogs
			SELECT src.AccountSerial, K' + @Iterator + ', D' + @Iterator + ', U' + @Iterator + ', T' + @Iterator + ', S' + @Iterator + ', ''K' + @Iterator + ''', ''tbl_AccountTrunk_Extend''
			FROM tbl_AccountTrunk_Extend src INNER JOIN #AccountSerials s ON src.AccountSerial = s.AccountSerial
			WHERE src.R' + @Iterator + ' = 1 AND src.K' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.CORA_ItemLogs
			SELECT src.AccountSerial, K' + @Iterator + ', D' + @Iterator + ', U' + @Iterator + ', T' + @Iterator + ', S' + @Iterator + ', ''K' + @Iterator + ''', ''tbl_AccountTrunk_Extend''
			FROM tbl_AccountTrunk_Extend src INNER JOIN #AccountSerials s ON src.AccountSerial = s.AccountSerial
			WHERE src.R' + @Iterator + ' = 2 AND src.K' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		SET @Iterator = @Iterator + 1
	END
	
	SET @Iterator = 0
	
	WHILE (@Iterator < 6)
	BEGIN
		EXEC('INSERT INTO RF_HISTORY.dbo.BELL_ItemLogs
			SELECT src_base.Serial, src_inven.EK' + @Iterator + ', src_inven.ED' + @Iterator + ', 0, src_inven.ET' + @Iterator + ', src_inven.ES' + @Iterator + ', ''EK' + @Iterator + ''', ''tbl_general''
			FROM (tbl_base src_base JOIN tbl_general src_inven ON src_base.Serial = src_inven.Serial)
			INNER JOIN #Serials s ON src_base.Serial = s.Serial 
			WHERE src_base.Race IN (0,1) AND src_inven.EK' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.ACC_ItemLogs
			SELECT src_base.Serial, src_inven.EK' + @Iterator + ', src_inven.ED' + @Iterator + ', 0, src_inven.ET' + @Iterator + ', src_inven.ES' + @Iterator + ', ''EK' + @Iterator + ''', ''tbl_general''
			FROM (tbl_base src_base JOIN tbl_general src_inven ON src_base.Serial = src_inven.Serial)
			INNER JOIN #Serials s ON src_base.Serial = s.Serial 
			WHERE src_base.Race IN (2,3) AND src_inven.EK' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		EXEC('INSERT INTO RF_HISTORY.dbo.CORA_ItemLogs
			SELECT src_base.Serial, src_inven.EK' + @Iterator + ', src_inven.ED' + @Iterator + ', 0, src_inven.ET' + @Iterator + ', src_inven.ES' + @Iterator + ', ''EK' + @Iterator + ''', ''tbl_general''
			FROM (tbl_base src_base JOIN tbl_general src_inven ON src_base.Serial = src_inven.Serial)
			INNER JOIN #Serials s ON src_base.Serial = s.Serial 
			WHERE src_base.Race = 4 AND src_inven.EK' + @Iterator + ' > -1
			INSERT INTO #Counts (StmtRowCount) VALUES (@@ROWCOUNT)')
		SET @Iterator = @Iterator + 1
	END
	
	SET @Iterator = 0
	
	SELECT @RecordsProcessed = SUM(StmtRowCount) FROM #Counts
	
	INSERT INTO RF_HISTORY.dbo.tbl_runs (date, records_processed, LastConnTime, records_deleted) VALUES (GETDATE(), @RecordsProcessed, @MaxLastConnTime, @RecordsDeleted)
	
END


GO


