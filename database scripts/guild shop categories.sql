USE [RF_GAMECP]
GO

/****** Object:  Table [dbo].[gamecp_g_shop_categories]    Script Date: 01/12/2017 23:22:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[gamecp_g_shop_categories](
	[cat_id] [int] IDENTITY(1,1) NOT NULL,
	[cat_sub_id] [int] NULL,
	[cat_order] [int] NULL,
	[cat_name] [varchar](255) NULL,
	[cat_description] [varchar](255) NULL,
 CONSTRAINT [PK_gamecp_g_shop_category] PRIMARY KEY CLUSTERED 
(
	[cat_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[gamecp_g_shop_categories] ADD  CONSTRAINT [DF_gamecp_g_shop_categories_cat_sub_id]  DEFAULT ((0)) FOR [cat_sub_id]
GO

ALTER TABLE [dbo].[gamecp_g_shop_categories] ADD  CONSTRAINT [DF_gamecp_g_shop_categories_cat_order]  DEFAULT ((0)) FOR [cat_order]
GO


