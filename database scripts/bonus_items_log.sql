USE [RF_GAMECP]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[gamecp_bonus_items_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[bonus_account_id] [int] NOT NULL,
	[bonus_char_id] [int] NOT NULL,
	[bonus_k] [int] NOT NULL,
	[bonus_u] [int] NOT NULL,
	[bonus_d] [bigint] NOT NULL,
	[bonus_t] [int] NOT NULL,
	[bonus_time] [varchar](255) NULL,
	[bonus_pp_trans] [varchar](255) NULL,
 CONSTRAINT [PK_gamecp_bonus_items_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
