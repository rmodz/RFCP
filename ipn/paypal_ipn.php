<?php

define("BASE_PATH", "..");
define("IN_GAMECP_SALT58585", true);
include( BASE_PATH . "/core/common.php" );

$res_status = true;
$time = date("F j Y G:i");
$timestamp = time();

$sandbox = false;
$domain = !$sandbox ? "www.paypal.com" : "www.sandbox.paypal.com";
$txn_id = isset($_POST['txn_id']) ? antiject($_POST['txn_id']) : "";
$payer_email = isset($_POST['payer_email']) ? antiject($_POST['payer_email']) : "";
$payer_id = isset($_POST['payer_id']) ? antiject($_POST['payer_id']) : "";
$business_email = isset($_POST['business']) ? antiject($_POST['business']) : "";
$custom = isset($_POST['custom']) ? antiject($_POST['custom']) : "1";
$payment_fee = isset($_POST['mc_fee']) ? $_POST['mc_fee'] : 0;
$payment_gross = isset($_POST['mc_gross']) ? $_POST['mc_gross'] : 0;
$payment_status = isset($_POST['payment_status']) ? antiject($_POST['payment_status']) : "";
$attempt = get_user_data($custom);

if ($attempt["error"] == True)
{
  gamecp_log(5, $custom, "PAYPAL - ERROR - Unable to find or query this user id");
  exit();
}
else if ($attempt["data"]["AccountName"] != "")
{
  $user_name = $attempt["data"]["AccountName"];
}
else
{
  $user_name = $custom;
  gamecp_log(5, $custom, "PAYPAL - ERROR - Could not look up account serial supplied by PayPal: {$custom}");
  gamecp_log(5, $custom, "PAYPAL - ERROR - Did not credit TXN ID: {$txn_id} | Payer ID: {$payer_id} | Payer Email: {$payer_email}");
  exit();
}

$log_message = "PAYPAL - LOADED PAYPAL.COM";
gamecp_log(0, $user_name, $log_message);

$cURL = curl_init();
$IPN = $_POST;
$IPN['cmd'] = '_notify-validate';
curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($cURL, CURLOPT_URL, "https://{$domain}/cgi-bin/webscr");
curl_setopt($cURL, CURLOPT_ENCODING, 'gzip');
curl_setopt($cURL, CURLOPT_BINARYTRANSFER, true);
curl_setopt($cURL, CURLOPT_POST, true); // POST back
curl_setopt($cURL, CURLOPT_POSTFIELDS, $IPN); // the $IPN
curl_setopt($cURL, CURLOPT_HEADER, false);
curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
curl_setopt($cURL, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
curl_setopt($cURL, CURLOPT_FORBID_REUSE, true);
curl_setopt($cURL, CURLOPT_FRESH_CONNECT, true);
curl_setopt($cURL, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($cURL, CURLOPT_TIMEOUT, 60);
curl_setopt($cURL, CURLINFO_HEADER_OUT, true);
curl_setopt($cURL, CURLOPT_HTTPHEADER, array (
  'Connection: close',
  'Expect: ',
));
$Response = curl_exec($cURL);
$Status = (int) curl_getinfo($cURL, CURLINFO_HTTP_CODE);
curl_close($cURL);

if ($Response == "VERIFIED")
{
  $res_status = "verified";
}
else if (strpos($Response, "INVALID") == 0)
{
  $res_status = "invalid";
}
else
{
  $res_status = false;
}

if ($res_status == "verified")
{
  $credits = calculate_credits($config['donations_credit_muntiplier'], $config['donations_number_of_pay_options'], $config['donations_start_price'], $config['donations_start_credits'], $payment_gross);
  if (!isset($credits) || $credits == "")
  {
    $credits = 0;
  }

  $credits_level = get_credits_level($config['donations_number_of_pay_options'], $config['donations_start_price'], $payment_gross);

  if ($payment_status == "Completed")
  {
    if ($business_email == $config['paypal_email'])
    {
      $sql = "SELECT tranid FROM gamecp_paypal WHERE tranid = ? ";
      $params = array ($txn_id);
      $tnx_query = sqlsrv_query(connectdb(RFCP), $sql, $params);
      if (sqlsrv_num_rows($tnx_query) == 0)
      {
        $sql = "INSERT INTO gamecp_paypal (tranid, amount, fee, userid, name, credits, time, payerid, payeremail, verified) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1)";
        $params = array ($txn_id, $payment_gross, $payment_fee, $custom, $custom,
          $credits, $time, $payer_id, $payer_email);
        $paypal_query = sqlsrv_query(connectdb(RFCP), $sql, $params);
        gamecp_log(0, $user_name, "PAYPAL - SUCCESSFULL PAYMENT - TXN ID: {$txn_id} | Amount: \$" . $payment_gross);

        //This should create the credits entry if it doesn't exist
        $attempt = get_user_credits(trim($custom));

        add_user_credits(trim($custom), $credits);
        gamecp_log(0, $user_name, "PAYPAL - ADDED CREDITS - UPDATE - TXN ID: {$txn_id} | Credits: {$credits}");

        //Add bonus items
        $bonus_enabled = isset($config['bonus_enabled']) ? ($config['bonus_enabled'] == 1) : False;
        if ($bonus_enabled)
        {
          try
          {
            $attempt2 = get_bonus_item_character($custom);
            if ($attempt2["char_serial"] != -1)
            {
              give_character_bonus_items($custom, $attempt2["char_serial"], $credits_level, $txn_id);
            }
          }
          catch (Exception $e)
          {
            gamecp_log(0, $user_name, "PAYPAL - BONUS FAILED - MESSAGE: " . $e -> getMessage());
          }
        }
      }
      else
      {
        $log_message = "PAYPAL - DUPLICATE TXN ID - TXN ID: {$txn_id} | PAYPAL EMAIL: {$payer_email} | STATUS: {$payment_status}";
        gamecp_log(5, $user_name, $log_message);
      }
    }
    else
    {
      $log_message = "PAYPAL - INVALID BUSINESS - TXN ID: {$txn_id} | PAYPAL EMAIL: {$payer_email} | STATUS: {$payment_status} | Business: {$business_email}";
      gamecp_log(5, $user_name, $log_message);
    }
  }
  else
  {
    if ($payment_status == "Reversed")
    {
      $log_message = "PAYPAL - <b>REVERSED</b> - TXN ID: {$txn_id} | PAYPAL EMAIL: {$payer_email} | Business: {$business_email}";
      gamecp_log(5, $user_name, $log_message);
      $ban_reason = "Auto - PayPal Reversal";
      $ban_period = 999;

      $attempt = ban_user($custom, $ban_period, $ban_reason, 0, $user_name);
      if ($attempt["error"] == True)
      {
        gamecp_log(5, $user_name, "PAYPAL - <b>NOT BANNED</b> - SQL ERROR while trying to ban for payment reversal", 1);
      }
      else
      {
        gamecp_log(5, $user_name, "PAYPAL - <b>BANNED</b> - Automatic ban for payment reversal", 1);
      }
    }
    else
    {
      if ($payment_status == "Canceled_Reversal")
      {
        $log_message = "PAYPAL - <b>CANCELED REVERSAL</b> - TXN ID: {$txn_id} | PAYPAL EMAIL: {$payer_email} | Business: {$business_email}";
        gamecp_log(5, $user_name, $log_message);
      }
      else
      {
        $log_message = "PAYPAL - <b>INCOMPLETE</b> - TXN ID: {$txn_id} | PAYPAL EMAIL: {$payer_email} | STATUS: {$payment_status} | Business: {$business_email}";
        gamecp_log(4, $user_name, $log_message);
      }
    }
  }
}
else if ($res_status == "invalid")
{
  $log_message = "PAYPAL - PAYMENT INVALID - PAYER ID: {$payer_id} | PAYPAL EMAIL: {$payer_email} - " . $Response;
  gamecp_log(5, $user_name, $log_message);
}
else
{
  $log_message = "PAYPAL - PAYMENT FAILED - Unknown Error - " . $Response;
  gamecp_log(1, $user_name, $log_message);
}
