<?php

define("BASE_PATH", "..");
define("IN_GAMECP_SALT58585", true);
include( BASE_PATH . "/core/common.php" );

require_once(BASE_PATH . '/includes/paymentwall.php');

$pingback = new Paymentwall_Pingback($_GET, $_SERVER['REMOTE_ADDR']);
if ($pingback -> validate())
{
  $productId = $pingback -> getProduct() -> getId();
  echo 'OK'; // Paymentwall expects response to be OK, otherwise the pingback will be resent
}
else
{
  echo $pingback -> getErrorSummary();
}
